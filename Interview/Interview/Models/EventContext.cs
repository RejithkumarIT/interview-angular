﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace Interview.Models
{
    public class EventContext
    {
        public string ConnectionString { get; set; }

        public EventContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<EventModel> GetAllEvents()
        {
            List<EventModel> list = new List<EventModel>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * from events where status ='1'", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new EventModel()
                        {
                            id          = Convert.ToInt32(reader["id"]),
                            event_title = reader["event_title"].ToString(),
                            seo_title   = reader["seo_title"].ToString(),
                            //Price     = Convert.ToInt32(reader["Price"]),
                            description  = reader["description"].ToString()
                        });
                    }
                }
            }
            return list;
        }
    }
}
