﻿
namespace Interview.Models
{
    public class EventModel
    {
        
        public int id { get; set; }

        public string event_title { get; set; }

        public string seo_title { get; set; }

        public string description { get; set; }
    }
}
