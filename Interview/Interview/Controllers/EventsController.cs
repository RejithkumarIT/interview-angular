﻿using Interview.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interview.Controllers
{
    public class EventsController : Controller
    {
        public IActionResult Index()
        {
            EventContext context = HttpContext.RequestServices.GetService(typeof(Interview.Models.EventContext)) as EventContext;
            return View(context.GetAllEvents());
        }
    }
}
