﻿Create procedure [dbo].[AddNewEvent]  
(  
   @event_name NVARCHAR (50),  
   @event_location NVARCHAR (50),  
   @event_link NVARCHAR (50),
   @created_by INT ,
   @created_date TIMESTAMP
)  
as  
begin  
   Insert into events values(@event_name,@event_location,@event_link,@created_by,@created_date)  
End