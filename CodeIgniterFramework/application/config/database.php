<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once __DIR__."/../../env.php";
$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	   => 'mysql:host='.DB_HOST.';dbname='.DB_NAME,
	'hostname' => 'mysql:host='.DB_HOST,
	'username' => DB_USER,
	'password' => DB_PASSWD,
	'database' => DB_NAME,
	'dbdriver' => 'pdo',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);