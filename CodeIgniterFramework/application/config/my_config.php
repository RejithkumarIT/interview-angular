<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['upload_path']                                      = BASEPATH;

$config['gender']                                           = array(0=>'Male', 1=>'Female', 2=>'Transgender');
$config['user_title']                                       = array(0 => 'Dr',  1 => 'Master', 2 =>'Miss', 3 => 'Mr', 4 => 'Mrs', 5 =>'Ms', 6 => 'Prof', 7 => 'Rev');
$config['toll_free']                                        = "xxxxxxxx";
$config['default_mail']                                     = 'xxxxxxxx';
$config['default_mail_name']                                = 'xxxxxxxx';
$config['email_confirmation_days']                          = 1;
$config['sms_confirmation_days']                            = 30;
$config['image_file_types']                                 = array('png', 'jpg', 'jpeg');
$config['doc_file_types']                                   = array('pdf', 'doc', 'docx','rtf','txt','png', 'jpg', 'jpeg','avi','mp4','3gp', 'mov');
$config['image_thumb_size']                                 = 200;
$config['file_upload_size']                                 = 10000;

$config['disclaimer_content']                               = 'xxxxxxxx - This email and any files transmitted with it are confidential and contain privileged or copyright information. xxxxxxxx xxxxxxxx not xxxxxxxx this xxxxxxxx to another party without gaining permission from the sender. If you are not the intended recipient you must not copy, distribute or use this email or the information contained in it for any purpose other than to notify us.<br><br>If you have received this message in error, please notify the sender immediately, and delete this email from your system. We do not guarantee that this material is free from viruses or any other defects although due care has been taken to minimise the risk. Any views expressed in this message are those of the individual sender, except where the sender specifically states them to be the views of fghgf. <br><br>gf';

$config['site_address']                                     = 'xxxxxxxx';

$config['site_url']                                         = 'https://localhost/';
$config['support_and_contact']                              = 'xxxxxxxx';
$config['smptp_mail'] = Array(
    'protocol' => 'smtp',
    'smtp_host' => 'xxxxxxxx',
    'smtp_port' => 465,
    'smtp_user' => 'xxxxxxxx',
    'smtp_pass' => 'xxxxxxxx',
    'mailtype'  => 'xxxxxxxx',
    'charset'   => 'iso-8859-1'
);
$config['student_admin']                                    = 'xxxxxxxx';
$config['student_photo']                                    = 'assets/profile/';
$config['assets_path']                                      = 'assets/';
$config['access_name']                                      = array(0=>'No Access', 1=>'Viewer',2=>'Editor');

$config['donar_max_amount']                                 = '3000000';
$config['donar_amount']                                     = '2500000';
$config['donar_role_id']                                    = '10';
$config['donar_admin_role_id']                              = '9';
$config['it_admin']                                         = '6';
$config['fa_team']                                          = '11';
$config['careerteam']                                       = '2';
$config['careeradmin']                                      = '5';
$config['fa_percentage_access']                             = '13';
$config['SPREADSHEET_API_KEY']                              = 'AIzaSyAl2tzUf9m5Xe9gD-lP6NHczecM3Fa0gpY';
$config['INTERNSHIP_SHEET']                                 = '1q84tKSRbabZzgRIPeyfDMWUWcoN7VpIkX47KN08jCMg';
$config['STUDENT_DATA_SHEET']                               = '1VP0rz7FTcje3x3n3hsFzH1ZS80U2QIdwtZ0QIO3nLvM';
$config['DONAR_DATA_SHEET']                                 = '1VP0rz7FTcje3x3n3hsFzH1ZS80U2QIdwtZ0QIO3nLvM';
$config['EXTRA_CURRICULAR']                                 = '19n2qFJf_O9bDmMs0H4tBeiwVmYpQwL1eXgURJSlsaC8';
$config['ifmrgsb_school']                                   = '1';
$config['sias_school']                                      = '2';
$config['both_school']                                      = '3';

//ERP Temporary
$config['master_feedback_file']                             = 'assets/ERP/feedback-master.json';
$config['master_remainder_file']                            = 'assets/ERP/remainder-master.json';
$config['master_batch']                                     = 'assets/ERP/master-batch.json';
$config['master_term']                                      = 'assets/ERP/master-term.json';
$config['feedback_ifmrgsb']                                 = 'assets/ERP/feedback_ifmrgsb.json';
$config['remainder_ifmrgsb']                                = 'assets/ERP/remainder_ifmrgsb.json';

$config['driveUploadURL']                                   = 'https://script.google.com/macros/s/AKfycbwX6N_39PjkqoVAK8qws_dqmVZF0mZPzl11hhVMP7DFUisStFYT/exec';
$config['viewDriveImage']                                   = 'https://drive.google.com/thumbnail?id=';
$config['downloadDriveImage']                               = 'https://drive.google.com/uc?export=download&id=';

