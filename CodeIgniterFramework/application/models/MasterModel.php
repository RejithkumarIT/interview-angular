<?php

header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed 

class MasterModel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url', 'form', 'date');
        $this->load->library('session');
    }

    function get_roles($data = array()) {
        $result = array();
        $this->db->from('roles');
        $select = '*';
        if (isset($data['select'])) {
            $select = $data['select'];
        }
        $this->db->select($select);
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
        }

        if (isset($data['validate_string'])) {
            $this->db->where('validate_string', $data['validate_string']);
        }
        
        if (isset($data['parent_id'])) {
            $this->db->where('parent_id', $data['parent_id']);
        }

        if (isset($data['role'])) {
            $this->db->where('role', $data['role']);
        }

        $this->db->where('status', '1');
        $query = $this->db->get();
        /* $str = $this->db->last_query();
          echo $str; */
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

    function insert_roles($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('roles', $data);
            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_roles($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {
            if (isset($condition['id'])) {
                $this->db->where('id', $condition['id']);
            }

            $this->db->update('roles', $data);

            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }

    function get_all_status($data = array()) {
        $result = array();
        $this->db->from('all_status');
        $select = '*';
        if (isset($data['select'])) {
            $select = $data['select'];
        }
        $this->db->select($select);
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
        }
        $this->db->where('status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function insert_all_status($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('all_status', $data);
            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_all_status($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {
            if (isset($condition['id'])) {
                $this->db->where('id', $condition['id']);
            }

            $this->db->update('all_status', $data);

            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }
    
    function get_school($data) {
        $select = '*';
        $result = array();
        if (count($data) > 0) {
            $this->db->from('school');
            if (isset($data['select'])) {
                $select = $data['select'];
            }
            $this->db->select($select);

            if (isset($data['status'])) {
                $this->db->where('status', $data['status']);
            }

            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
            }

            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
            }
        }
        return $result;
    }

    function insert_school($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('school', $data);
            /*$str = $this->db->last_query();
             echo $str; exit;*/
            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_school($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {
            if (isset($condition['id'])) {
                $this->db->where('id', $condition['id']);
            }
            

            $this->db->update('school', $data);

            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }

    function delete_school($condition){
        $result = false;
        if(count($condition) > 0){
            if(isset($condition['id'])){
                $this->db->where('id', $condition['id']);
            }
            
            $this->db->delete('school');
            $affected_rows = $this->db->affected_rows();
            if($affected_rows > 0){
                $result = true;
            } 
        }
        return $result;
    }
}
