<?php

header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed 

class Menus_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function get_my_menus($data) {
        $select = '*';
        $result = array();
        if (count($data) > 0) {
            $this->db->from('menus');
            if (isset($data['select'])) {
                $select = $data['select'];
            }
            $this->db->select($select);

            if (isset($data['status'])) {
                $this->db->where('status', $data['status']);
            }

            if (isset($data['parent_id'])) {
                $this->db->where('parent_id', $data['parent_id']);
            }

            if (isset($data['order'])) {
                $this->db->where('order', $data['order']);
            }

            if (isset($data['is_parent'])) {
                $this->db->where('is_parent', $data['is_parent']);
            }

            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
            }

            $query = $this->db->get();
//                    $str = $this->db->last_query();
//                    echo "<pre>".$str."</pre>"; 
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
            }
        }
        return $result;
    }
    
    //menus...
    function insert_menus($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('menus', $data);
            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_menus($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {
            if (@$condition['id']) {
                $this->db->where('id', $condition['id']);
            }
            $this->db->update('menus', $data);
            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }
    
    function delete_menus($condition){
        $result = false;
        if(count($condition) > 0){
            if(isset($condition['id'])){
                $this->db->where('id', $condition['id']);
            }
            $this->db->delete('menus');
            $affected_rows = $this->db->affected_rows();
            if($affected_rows > 0){
                $result = true;
            } 
        }
        return $result;
    }
    //menu_urls...
    function insert_menu_urls($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('menus_urls', $data);

            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_menu_urls($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {

            if (isset($condition['id'])) {
                $this->db->where('id', $condition['id']);
            }
            if (isset($condition['menu_id'])) {
                $this->db->where('menu_id', $condition['menu_id']);
            }
            $this->db->update('menus', $data);

            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }

    function check_edit_menus($data = array()) {
        $result = array();

        if (count($data) > 0) {
            $this->db->select('Menus.*');
            $this->db->from('menus as Menus');

            if (isset($data['status'])) {
                $this->db->where('Menus.status', $data['status']);
            }
            if (isset($data['parent_id'])) {
                $this->db->where('Menus.parent_id !=', $data['parent_id'], FALSE);
            }
            if (isset($data['created_by'])) {
                $this->db->where('Menus.created_by', $data['created_by']);
            }
            if (isset($data['is_function'])) {
                $this->db->where('Menus.is_function', $data['is_function']);
            }
            if (isset($data['id'])) {
                $this->db->where('Menus.id !=', $data['id'], FALSE);
            }
            if (isset($data['menu_name'])) {
                $this->db->like('Menus.menu_name', strtolower($data['menu_name']));
            }
            $this->db->order_by('Menus.menu_name', 'ASC');

            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                $result = $query->result_array();
            }
        }
        return $result;
    }

    function get_menus($data = array()) {
        $result = array();
        $select = '*';
        if (isset($data['select'])) {
            $select = $data['select'];
        }
        $this->db->select($select);
        $this->db->from('menus');

        if (isset($data['status'])) {
            $this->db->where('status', $data['status']);
        }
        if (isset($data['parent_id'])) {
            $this->db->where('parent_id', $data['parent_id']);
        }
        if (isset($data['created_by'])) {
            $this->db->where('created_by', $data['created_by']);
        }
        if (isset($data['is_parent'])) {
            $this->db->where('is_parent', $data['is_parent']);
        }
        if (isset($data['id'])) {
            $this->db->where('id', $data['id']);
        }
        if (isset($data['menu_name'])) {
            $this->db->like('menu_name', strtolower($data['menu_name']));
        }

        if (isset($data['action'])) {
            $this->db->like('action', strtolower($data['action']));
        }

        $this->db->order_by('order', 'ASC');

        $query = $this->db->get();
//        $str = $this->db->last_query();
//        /* echo "<pre>".$str."</pre>"; */
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

    //Get menus url details
    function get_menus_url_details($data = array()) {
        $result = array();

        $this->db->select('Menus.*, MenuUrls.id AS url_id, MenuUrls.action');
        $this->db->from('menus as Menus');
        $this->db->join('menu_urls as MenuUrls', 'MenuUrls.menu_id = Menus.id', 'INNER');

        if (isset($data['status'])) {
            $this->db->where('Menus.status', $data['status']);
        }
        if (isset($data['parent_id'])) {
            $this->db->where('Menus.parent_id', $data['parent_id']);
        }
        if (isset($data['created_by'])) {
            $this->db->where('Menus.created_by', $data['created_by']);
        }
        if (isset($data['url_id'])) {
            $this->db->where('MenuUrls.id', $data['url_id']);
        }
        if (isset($data['id'])) {
            $this->db->where('Menus.id', $data['id']);
        }
        if (isset($data['menu_name'])) {
            $this->db->like('Menus.menu_name', strtolower($data['menu_name']));
        }
        $this->db->order_by('Menus.menu_name', 'ASC');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

    function get_menus_details($condition = array()) {
        $result = array();

        $this->db->select('Menus.*, MenuUrls.id AS url_id, MenuUrls.action');
        $this->db->from('menus AS Menus');
        $this->db->join('menu_urls as MenuUrls', 'MenuUrls.menu_id = Menus.id', 'INNER');

        //Conditions
        if (isset($condition['status'])) {
            $this->db->where('Menus.status', $condition['status']);
        }
        $this->db->where('Menus.parent_id', 0);

        $this->db->order_by('Menus.order, Menus.menu_name', 'ASC');
        $main_menu = $this->db->get();

        if ($main_menu->num_rows() > 0) {
            #$result = $query->result_array();			
            foreach ($main_menu->result_array() as $row) {
                $this->db->select('Menus.*, MenuUrls.id AS url_id, MenuUrls.action');
                $this->db->from('menus AS Menus');
                $this->db->join('menu_urls as MenuUrls', 'MenuUrls.menu_id = Menus.id', 'INNER');

                if (isset($condition['status'])) {
                    $this->db->where('Menus.status', $condition['status']);
                }
                $this->db->where('Menus.parent_id', $row['id']);
                $this->db->order_by('Menus.order, Menus.menu_name', 'ASC');

                $child_menu = $this->db->get();
                if ($child_menu->num_rows() > 0) {
                    $row['child'] = $child_menu->result_array();
                }
                array_push($result, $row);
            }
        }
        return $result;
    }

    function get_role_permissions($condition = array()) {
        $result = array();

        $this->db->select('Menus.id,Menus.action,Menus.menu_name,Menus.parent_id,Menus.order,Menus.is_function,Menus.status');
        $this->db->from('menus as Menus');
        //$this->db->join('menu_urls as MenuUrls', 'MenuUrls.menu_id = Menus.id','INNER');
        $this->db->join('users_menu_permissions as MenuPermissions', 'MenuPermissions.menus_id = Menus.id', 'INNER');

        if (isset($condition['status'])) {
            $this->db->where('Menus.status', $condition['status']);
            $this->db->where('MenuPermissions.status', $condition['status']);
        }

        if (isset($condition['roles'])) {
            $this->db->where_in('MenuPermissions.roles_id', $condition['roles']);
        }

        if (isset($condition['menus_id'])) {
            $this->db->where_in('MenuPermissions.menus_id', $condition['menus_id']);
        }

        if (isset($condition['users_id'])) {
            $this->db->where('MenuPermissions.users_id', $condition['users_id']);
        }

        if (isset($condition['sub_roles_id'])) {
            $this->db->where('MenuPermissions.sub_roles_id', $condition['sub_roles_id']);
        }

        $this->db->group_by('Menus.id');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    //insert_update_role_menus... for menu_permissions
    function menu_permissions($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->select('id');
            $this->db->from('users_menu_permissions');
            if (isset($data['roles_id'])) {
                $this->db->where('roles_id', $data['roles_id']);
            }

            if (isset($data['sub_roles_id'])) {
                $this->db->where('sub_roles_id', $data['sub_roles_id']);
            }

            if (isset($data['menus_id'])) {
                $this->db->where('menus_id', $data['menus_id']);
            }
            if (isset($data['role_check'])) {
                $this->db->where('roles_id', $data['roles_id_ch']);
                $this->db->or_where('sub_roles_id', $data['sub_roles_id_ch']);
            }

            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $result_data = $query->row_array();
                if ($result_data['id']) {
                    if (isset($data['created_by'])) {
                        unset($data['created_by']);
                    }
                    if (isset($data['created_date'])) {
                        unset($data['created_date']);
                    }
                    if (isset($data['role_check'])) {
                        unset($data['role_check']);
                        unset($data['roles_id_ch']);
                        unset($data['sub_roles_id_ch']);
                    }

                    $this->db->where('id', $result_data['id']);
                    $this->db->update('users_menu_permissions', $data);

                    if ($this->db->affected_rows() > 0) {
                        $data['id'] = $result_data['id'];
                        $result = $data;
                    }
                }
            } else {
                if (isset($data['role_check'])) {
                    unset($data['role_check']);
                    unset($data['roles_id_ch']);
                    unset($data['sub_roles_id_ch']);
                }
                $this->db->insert('users_menu_permissions', $data);
                if ($this->db->affected_rows() > 0) {
                    $id = $this->db->insert_id();
                    $data['id'] = $id;
                    $result = $data;
                }
            }
        }
        return $result;
    }

    //menu_permissions
    function get_menu_permissions($data = array()) {
        $result = array();

        $this->db->select('MenuPermissions.*');
        $this->db->from('users_menu_permissions as MenuPermissions');

        if (isset($data['status'])) {
            $this->db->where('MenuPermissions.status', $data['status']);
        }
        if (isset($data['roles_id'])) {
            $this->db->where('MenuPermissions.roles_id', $data['roles_id']);
        }

        if (isset($data['users_id'])) {
            $this->db->where('MenuPermissions.users_id', $data['users_id']);
        }
        if (isset($data['menus_id'])) {
            $this->db->where('MenuPermissions.menus_id', $data['menus_id']);
        }
        if (isset($data['created_by'])) {
            $this->db->where('MenuPermissions.created_by', $data['created_by']);
        }
        if (isset($data['role_check'])) {
            $this->db->group_start();
            $this->db->where('MenuPermissions.roles_id', $data['roles_id_ch']);
            //$this->db->or_where('MenuPermissions.sub_roles_id',$data['sub_roles_id_ch']);
            $this->db->group_end();
        }

        if (isset($data['id'])) {
            $this->db->where('MenuPermissions.id', $data['id']);
        }

        $query = $this->db->get();
        /* $str = $this->db->last_query();
          echo $str; */
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

    function insert_users_menu_permissions($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('users_menu_permissions', $data);
            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_users_menu_permissions($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {
            if (isset($condition['id'])) {
                $this->db->where('id', $condition['id']);
            }
            if (isset($data['roles_id'])) {
                $this->db->where('roles_id', $data['roles_id']);
            }

            if (isset($data['users_id'])) {
                $this->db->where('users_id', $data['users_id']);
            }

            $this->db->update('users_menu_permissions', $data);

            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }

    //Get Child menu url id to Parent menu ul id...
    function get_child_menu_parent_menu_url_data($condition = array()) {
        $result = array();

        $this->db->select('Menus.id, Menus.parent_id, ParentMenuUrls.id as pmu_id');
        $this->db->from('menu_urls as MenuUrls');
        $this->db->join('menus as Menus', 'MenuUrls.menu_id = Menus.id', 'INNER');
        $this->db->join('menu_urls as ParentMenuUrls', 'ParentMenuUrls.menu_id = Menus.parent_id', 'INNER');

        if (isset($condition['menu_url_id'])) {
            $this->db->where('MenuUrls.id', $condition['menu_url_id']);
        }

        $this->db->where('Menus.parent_id !=', 0);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

}

?>