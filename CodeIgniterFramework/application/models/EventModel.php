<?php

header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed 

class EventModel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url', 'form', 'date');
        $this->load->library('session');
    }

    function get_events($data) {
        $select = '*';
        $result = array();
        if (count($data) > 0) {
            $this->db->from('events');
            if (isset($data['select'])) {
                $select = $data['select'];
            }

            $this->db->select($select);

            if (isset($data['status'])) {
                $this->db->where('status', $data['status']);
            }

            if (isset($data['is_stipend'])) {
                $this->db->where('is_stipend', $data['is_stipend']);
            }

            if (isset($data['event_type'])) {
                $this->db->where('event_type', $data['event_type']);
            }

            if (isset($data['seo_title'])) {
                $this->db->where('seo_title', $data['seo_title']);
            }

            if (isset($data['event_school'])) {
                $this->db->where('event_school', $data['event_school']);
            }

            if (isset($data['is_duplicate'])) {
                $this->db->where('is_duplicate', $data['is_duplicate']);
            }

            if (isset($data['id_in'])) {
                $arr = array_values($data['id_in']);
                $string = implode(",", $arr);
                $ids = array_map('intval', explode(',', $string));
                $this->db->where_in('id', $ids);
            }

            if (isset($data['login'])) {
                $this->db->where('email', $data['email_login']);
                $this->db->where('oauth_uid', $data['oauth_uid']);
            }


            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
            }
            
            if (isset($data['group_by']) && isset($data['group_by'])) {
                $this->db->group_by(array($data['group_by']));
            }
            
            if (isset($data['limit']) && isset($data['start'])) {
                $this->db->limit($data['limit'], $data['start']);
            }
            
            if (isset($data['keyword']) && $data['keyword']!='') {
                $keyword = trim(preg_replace('/[\t\n\r\s]+/', ' ', $data['keyword'])); //preg_replace('/\s+/', ' ',$data['keyword']);
                $this->db->group_start();
                $this->db->like('TRIM(timeslots)', $keyword, 'both');/*
                $this->db->or_like('TRIM(personal.last_name)', $keyword, 'both');
                $this->db->or_like('TRIM(personal.fullname)', $keyword, 'both');
                $this->db->or_like('TRIM(academic.roll_no)', $keyword, 'both');
                $this->db->or_like('TRIM(users.email)', $keyword, 'both');*/
                $this->db->group_end();
            }
            $this->db->where('is_deleted', '0');
            if (isset($data['order_by']) && $data['order_by']!='') {
                $this->db->order_by("id", "DESC");
            }
            
            $query = $this->db->get();
             /*$str = $this->db->last_query();
             echo $str; exit;*/
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
            }
        }
        return $result;
    }

    function insert_events($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('events', $data);
            $str = $this->db->last_query();
             echo $str; exit;
            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_events($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {
            if (isset($condition['id'])) {
                $this->db->where('id', $condition['id']);
            }
            if (isset($condition['email'])) {
                $this->db->where('email', $condition['email']);
            }

            $this->db->update('events', $data);
             $str = $this->db->last_query();
             echo $str; exit;
            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }

    

    //events_timeslot
    function get_events_timeslot($data) {
        $select = '*';
        $result = array();
        if (count($data) > 0) {
            $this->db->from('events_timeslot');
            if (isset($data['select'])) {
                $select = $data['select'];
            }
            $this->db->select($select);

            if (isset($data['status'])) {
                $this->db->where('status', $data['status']);
            }

            if (isset($data['date_check'])) {
                $this->db->where('date >=', $data['date_check']);
            }

            if (isset($data['date'])) {
                $this->db->where('date', $data['date']);
            }

            if (isset($data['events_id'])) {
                $this->db->where('events_id', $data['events_id']);
            }
            
            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
            }

            if (isset($data['keyword']) && $data['keyword']!='') {
                $keyword = trim(preg_replace('/[\t\n\r\s]+/', ' ', $data['keyword'])); //preg_replace('/\s+/', ' ',$data['keyword']);
                $this->db->group_start();
                $this->db->like('TRIM(timeslots)', $keyword, 'both');/*
                $this->db->or_like('TRIM(personal.last_name)', $keyword, 'both');
                $this->db->or_like('TRIM(personal.fullname)', $keyword, 'both');
                $this->db->or_like('TRIM(academic.roll_no)', $keyword, 'both');
                $this->db->or_like('TRIM(users.email)', $keyword, 'both');*/
                $this->db->group_end();
            }
            if (isset($data['group_date'])) {
                $this->db->group_by('DATE(date)');
            }
            
            $query = $this->db->get();
            /*$str = $this->db->last_query();
             echo $str; exit;*/
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
            }
        }
        return $result;
    }

    function insert_events_timeslot($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('events_timeslot', $data);
            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_events_timeslot($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {
            if (isset($condition['id'])) {
                $this->db->where('id', $condition['id']);
            }
            if (isset($condition['events_id'])) {
                $this->db->where('events_id', $condition['events_id']);
            }

            $this->db->update('events_timeslot', $data);

            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }

    function delete_events_timeslot($condition){
        $result = false;
        if(count($condition) > 0){
            if(isset($condition['id'])){
                $this->db->where('id', $condition['id']);
            }

            if(isset($condition['events_id'])){
                $this->db->where('events_id', $condition['events_id']);
            }
            
            $this->db->delete('events_timeslot');
            $affected_rows = $this->db->affected_rows();
            if($affected_rows > 0){
                $result = true;
            } 
        }
        return $result;
    }

    function get_event_category($data) {
        $select = '*';
        $result = array();
        if (count($data) > 0) {
            $this->db->from('event_category');
            if (isset($data['select'])) {
                $select = $data['select'];
            }
            $this->db->select($select);

            if (isset($data['status'])) {
                $this->db->where('status', $data['status']);
            }

            if (isset($data['email'])) {
                $this->db->where('email', $data['email']);
            }

            if (isset($data['mail_enable'])) {
                $this->db->where('mail_enable', $data['mail_enable']);
            }
            
            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
            }

            if (isset($data['group_date'])) {
                $this->db->group_by('DATE(date)');
            }
            
            $query = $this->db->get();
            /*$str = $this->db->last_query();
             echo $str; exit;*/
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
            }
        }
        return $result;
    }

    function insert_event_category($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('event_category', $data);
            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_event_category($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {
            if (isset($condition['id'])) {
                $this->db->where('id', $condition['id']);
            }

            $this->db->update('event_category', $data);

            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }

    function delete_event_category($condition){
        $result = false;
        if(count($condition) > 0){
            if(isset($condition['id'])){
                $this->db->where('id', $condition['id']);
            }
            
            $this->db->delete('event_category');
            $affected_rows = $this->db->affected_rows();
            if($affected_rows > 0){
                $result = true;
            } 
        }
        return $result;
    }
    //event_subscription 
}