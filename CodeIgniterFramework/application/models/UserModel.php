<?php

header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed 

class UserModel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url', 'form', 'date');
        $this->load->library('session');
    }

    function get_users($data) {
        $select = '*';
        $result = array();
        if (count($data) > 0) {
            $this->db->from('users');
            if (isset($data['select'])) {
                $select = $data['select'];
            }

            $this->db->select($select);

            if (isset($data['status'])) {
                $this->db->where('status', $data['status']);
            }

            if (isset($data['email'])) {
                $this->db->where('email', $data['email']);
            }

            if (isset($data['seo_title'])) {
                $this->db->where('seo_title', $data['seo_title']);
            }

            if (isset($data['role'])) {
                $this->db->where('role', $data['role']);
            }

            if (isset($data['id_in'])) {
                $arr = array_values($data['id_in']);
                $string = implode(",", $arr);
                $ids = array_map('intval', explode(',', $string));
                $this->db->where_in('id', $ids);
            }

            if (isset($data['login'])) {
                $this->db->where('email', $data['email_login']);
                $this->db->where('oauth_uid', $data['oauth_uid']);
            }

            if (isset($data['is_change'])) {
                $this->db->where('is_change', $data['is_change']);
            }
            
            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
            }

            if (isset($data['school'])) {
                $this->db->where('school', $data['school']);
            }
            if (isset($data['role_not'])) {
                $this->db->where('role !=', $data['role_not']);
            }
            
            if (isset($data['order_by_order']) && isset($data['order_by_order'])) {
                $this->db->order_by("faculty_order", "asc");
            }

            if (isset($data['group_by']) && isset($data['group_by'])) {
                $this->db->group_by(array($data['group_by']));
            }
            
            if (isset($data['limit']) && isset($data['start'])) {
                $this->db->limit($data['limit'], $data['start']);
            }

            
            $this->db->where('is_deleted', '0');
            $query = $this->db->get();
//             $str = $this->db->last_query();
//             echo $str; exit;
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
            }
        }
        return $result;
    }

    function insert_users($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('users', $data);
            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_users($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {
            if (isset($condition['id'])) {
                $this->db->where('id', $condition['id']);
            }
            if (isset($condition['email'])) {
                $this->db->where('email', $condition['email']);
            }

            $this->db->update('users', $data);

            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }

   

    function users_activies_data($data, $type = 'web') {
        $result = array();
        if (count($data) > 0) {
            $this->db->from('users_activies');
            $this->db->select('id, users_id, app_current_date, tool_current_date, web_current_date');
            $this->db->where('users_id', $data['users_id']);
            $this->db->order_by('id', 'ASC');
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $result = $query->row_array();
                $ua_id = $result['id'];
                if ($type == 'web') {
                    $data['web_last_used_date'] = $result['web_current_date'];
                } else if ($type == 'app') {
                    $data['app_last_used_date'] = $result['app_current_date'];
                } else if ($type == 'tool') {
                    $data['tool_last_sync_date'] = $result['tool_current_date'];
                }

                $this->db->where('id', $ua_id);
                $this->db->update('users_activies', $data);
                if ($this->db->affected_rows() > 0) {
                    $result = $data;
                }
            } else {
                if ($type == 'web') {
                    $data['web_last_used_date'] = '';
                } else if ($type == 'app') {
                    $data['app_last_used_date'] = '';
                } else if ($type == 'tool') {
                    $data['tool_last_sync_date'] = '';
                }
                $this->db->insert('users_activies', $data);

                if ($this->db->affected_rows() > 0) {
                    $result = $data;
                }
            }
        }
        return $result;
    }

    function insert_users_token($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->from('users_token');
            $this->db->select('id, users_id');
            $this->db->where('users_id', $data['users_id']);
            $this->db->order_by('id', 'ASC');
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $result = $query->row_array();
                $ut_id = $result['id'];
                $this->db->where('id', $ut_id);
                $this->db->update('users_token', $data);
                if ($this->db->affected_rows() > 0) {
                    $result = $data;
                }
            } else {
                $this->db->insert('users_token', $data);

                if ($this->db->affected_rows() > 0) {
                    $result = $data;
                }
            }
        }
        return $result;
    }

    

    function get_user_roles($data) {
        $select = '*';
        $result = array();
        if (count($data) > 0) {
            $this->db->from('user_roles');
            if (isset($data['select'])) {
                $select = $data['select'];
            }
            $this->db->select($select);

            if (isset($data['status'])) {
                $this->db->where('status', $data['status']);
            }

            if (isset($data['roles_id'])) {
                $this->db->where('roles_id', $data['roles_id']);
            }

            if (isset($data['users_id'])) {
                $this->db->where('users_id', $data['users_id']);
            }

            if (isset($data['is_primary'])) {
                $this->db->where('is_primary', $data['is_primary']);
            }

            if (isset($data['id'])) {
                $this->db->where('id', $data['id']);
            }

            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
            }
        }
        return $result;
    }

    function insert_user_roles($data) {
        $result = array();
        if (count($data) > 0) {
            $this->db->insert('user_roles', $data);
            if ($this->db->affected_rows() > 0) {
                $id = $this->db->insert_id();
                $data['id'] = $id;
                $result = $data;
            }
        }
        return $result;
    }

    function update_user_roles($data, $condition) {
        $result = false;
        if ((count($data) > 0) AND (count($condition) > 0)) {
            if (isset($condition['id'])) {
                $this->db->where('id', $condition['id']);
            }
            if (isset($condition['users_id'])) {
                $this->db->where('users_id', $condition['users_id']);
            }

            if (isset($condition['sub_roles_id'])) {
                $this->db->where('sub_roles_id', $condition['sub_roles_id']);
            }

            $this->db->update('user_roles', $data);

            if ($this->db->affected_rows() > 0) {
                $result = true;
            }
        }
        return $result;
    }
}
