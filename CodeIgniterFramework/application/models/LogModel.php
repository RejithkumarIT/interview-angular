<?php
header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed 
class LogModel extends CI_Model{
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url','form','date');
		$this->load->library('session');
	}

	function get_events_log($data){
		$select = '*';
		$result	= array();
		if(count($data) > 0){
			$this->db->from('events_log');
			if(isset($data['select'])){
				$select = $data['select'];
			}

			$this->db->select($select);

			if(isset($data['status'])){
				$this->db->where('status',$data['status']);
			} 

			if(isset($data['events_id'])){
				$this->db->where('events_id',$data['events_id']);
			}

			if(isset($data['id'])){
				$this->db->where('id',$data['id']);
			}
			$query 	= $this->db->get();
			
			if($query->num_rows() > 0){
				$result = $query->result_array();
			}
		}
		return $result;
	}


	function insert_events_log($data){
		$result = array();
		if(count($data) > 0){			
			$this->db->insert('events_log', $data);			
			if($this->db->affected_rows() > 0){
				$id 		= $this->db->insert_id();
				$data['id'] = $id;
				$result 	= $data;
			}
		}
		return $result;
	}

	function update_events_log($data, $condition){
		$result = false;
		if((count($data) > 0) AND (count($condition) > 0)){
			if(isset($condition['id'])){
				$this->db->where('id', $condition['id']);
			}	
			if(isset($condition['events_id'])){
				$this->db->where('events_id', $condition['events_id']);
			}

			$this->db->update('events_log', $data);

			if($this->db->affected_rows() > 0){
				$result = true;
			}
		}
		return $result;
	}

	function get_users_log($data){
		$select = '*';
		$result	= array();
		if(count($data) > 0){
			$this->db->from('users_log');
			if(isset($data['select'])){
				$select = $data['select'];
			}

			$this->db->select($select);

			if(isset($data['status'])){
				$this->db->where('status',$data['status']);
			} 

			if(isset($data['users_id'])){
				$this->db->where('users_id',$data['users_id']);
			}

			if(isset($data['id'])){
				$this->db->where('id',$data['id']);
			}
			$query 	= $this->db->get();
			
			if($query->num_rows() > 0){
				$result = $query->result_array();
			}
		}
		return $result;
	}


	function insert_users_log($data){
		$result = array();
		if(count($data) > 0){			
			$this->db->insert('users_log', $data);			
			if($this->db->affected_rows() > 0){
				$id 		= $this->db->insert_id();
				$data['id'] = $id;
				$result 	= $data;
			}
		}
		return $result;
	}

	function update_users_log($data, $condition){
		$result = false;
		if((count($data) > 0) AND (count($condition) > 0)){
			if(isset($condition['id'])){
				$this->db->where('id', $condition['id']);
			}	
			if(isset($condition['users_id'])){
				$this->db->where('users_id', $condition['users_id']);
			}

			$this->db->update('users_log', $data);

			if($this->db->affected_rows() > 0){
				$result = true;
			}
		}
		return $result;
	}

}    