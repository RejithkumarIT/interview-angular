<?php return array (
  'sans-serif' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir . '\ZapfDingbats',
    'bold' => $fontDir . '\ZapfDingbats',
    'italic' => $fontDir . '\ZapfDingbats',
    'bold_italic' => $fontDir . '\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir . '\Symbol',
    'bold' => $fontDir . '\Symbol',
    'italic' => $fontDir . '\Symbol',
    'bold_italic' => $fontDir . '\Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir . '\DejaVuSans-Bold',
    'bold_italic' => $fontDir . '\DejaVuSans-BoldOblique',
    'italic' => $fontDir . '\DejaVuSans-Oblique',
    'normal' => $fontDir . '\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir . '\DejaVuSansMono-Bold',
    'bold_italic' => $fontDir . '\DejaVuSansMono-BoldOblique',
    'italic' => $fontDir . '\DejaVuSansMono-Oblique',
    'normal' => $fontDir . '\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir . '\DejaVuSerif-Bold',
    'bold_italic' => $fontDir . '\DejaVuSerif-BoldItalic',
    'italic' => $fontDir . '\DejaVuSerif-Italic',
    'normal' => $fontDir . '\DejaVuSerif',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir . '\2e7b7733b9544200e274a25e7e62633c',
  ),
  'font' => array(
    'bold' => $fontDir . '/3aa102fd810d7c21aabb4d53e4b46374',
  ),
) ?>