<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EncryptionFunction {
    var $key        = 'AA74CINTERN2BBRT9GDYE84735136HEA7B63C27';
    var $IV         = '5fgf5HJ5g8f8de258';
    var $key_String = 'AA74CINTERNEDU2BBRT9GDYE84735136HEA7B63C27';
    var $IV_String  = '5fgf5HJ5g8f8ab558';
    var $ciphering  = "AES-256-CBC";
    var $options    = 0;

    function aes_encrypt_string($string = "") {
        $key           = hash('sha256', $this->key);
        $iv            = substr(hash('sha256', $this->IV), 0, 16);
        $output        = openssl_encrypt($string, $this->ciphering, $key, $this->options, $iv);
        $encryption    = base64_encode($output);
        return $encryption;
    }

    function aes_decrypt_string($string = "") {
        $key        = hash('sha256', $this->key);
        $iv         = substr(hash('sha256', $this->IV), 0, 16);
        $output     = openssl_decrypt(base64_decode($string), $this->ciphering, $key, $this->options, $iv);
        return $output;
    }

    function aes_encrypt($string = "") {
        $key           = hash('sha256', $this->key_String);
        $iv            = substr(hash('sha256', $this->IV_String), 0, 16);
        $output        = openssl_encrypt($string, $this->ciphering, $key, $this->options, $iv);
        $encryption    = base64_encode($output);
        return $encryption;
    }

    function aes_decrypt($string = "") {
        $key        = hash('sha256', $this->key_String);
        $iv         = substr(hash('sha256', $this->IV_String), 0, 16);
        $output     = openssl_decrypt(base64_decode($string), $this->ciphering, $key, $this->options, $iv);
        return $output;
    }

}
?>