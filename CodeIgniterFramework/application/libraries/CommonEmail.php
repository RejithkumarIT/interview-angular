<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CommonEmail {
    private $CI;
    function __construct(){
        $this->CI = &get_instance();
    }

    function register_email($data = array()){
        $result = false;
        if(count($data) > 0){     
            $this->CI   = &get_instance();            
            $from_email = $data['from_email'];
            $from_name  = $data['from_name'];
            
            $this->CI->email->from($from_email, $from_name);
            $this->CI->email->to($data['to_email'], $data['to_name']);
            $this->CI->email->subject('Krea SIS User Access Confirmation');
            $this->CI->email->set_mailtype("html");
            //$active_url = base_url().'page/confirmation/'.$data['activation_code'];
            
            $myFile         = FCPATH.'Assets/Templates/registration.html';            
            $file_funs      = file($myFile);
            $content        = '';
            $mail_content   = '';
            if(count($file_funs) > 0){
                foreach($file_funs as $file_num => $file_fun) {
                    $file_fun = str_replace('$sname$', $data['to_name'], $file_fun);
                    $file_fun = str_replace('$email$', $data['to_email'], $file_fun); 
				//	$file_fun = str_replace('$degree$', $data['degree'], $file_fun);
                   // $file_fun = str_replace('$course$', $data['course'], $file_fun);
                    $file_fun = str_replace('$dob$', $data['password'], $file_fun);
                    $file_fun = str_replace('$disclaimer_content$', $this->CI->config->item('disclaimer_content'), $file_fun);
                    $file_fun = str_replace('$support_and_contact$', $this->CI->config->item('support_and_contact'), $file_fun);
                    $mail_content.= $file_fun;
                }
            }
            $content.= $mail_content;
            $this->CI->email->message($content);
            if($this->CI->email->send()){
                $result = true;
            }
        }
        return $result;
    }

    function event_add_email($data = array()){
        $result = false;
        if(count($data) > 0){     
            $this->CI   = &get_instance();            
            $from_email = 'rejithsham296@outlook.com';
            $from_name  = 'IT Team';
            
            $this->CI->email->from($from_email, $from_name);
            $this->CI->email->to($data['to_email'], $data['to_name']);
            $this->CI->email->subject('Krea SIS User Access Confirmation');
            $this->CI->email->set_mailtype("html");
            //$active_url = base_url().'page/confirmation/'.$data['activation_code'];
            
            $myFile         = FCPATH.'assets/mail/event-notification.html';            
            $file_funs      = file($myFile);
            $content        = '';
            $mail_content   = '';
            if(count($file_funs) > 0){
                foreach($file_funs as $file_num => $file_fun) {
                    $file_fun = str_replace('$sname$', $data['to_name'], $file_fun);
                    $file_fun = str_replace('$email$', $data['to_email'], $file_fun); 
                    $file_fun = str_replace('$invoice_details$', $data['invoice_details'], $file_fun);
                    $file_fun = str_replace('$dob$', $data['password'], $file_fun);
                    //$file_fun = str_replace('$disclaimer_content$', $this->CI->config->item('disclaimer_content'), $file_fun);
                    //$file_fun = str_replace('$support_and_contact$', $this->CI->config->item('support_and_contact'), $file_fun);
                    $mail_content.= $file_fun;
                }
            }
            $content.= $mail_content;
            $this->CI->email->message($content);
            if($this->CI->email->send()){
                $result = true;
            }
        }
        return $result;
    }

}
?>