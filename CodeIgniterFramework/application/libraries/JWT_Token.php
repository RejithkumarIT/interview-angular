<?php 
require_once(APPPATH.'third_party/token/autoload.php');
require_once __DIR__."/../../env.php";
use \Firebase\JWT\JWT;
class JWT_Token extends JWT
{
    var $secret_key      = 'AA74CKREAU2BBRT9GDYE84735136HEA7B63C27';
    var $issuer_claim    = websiteURL;
    var $audience_claim  = websiteURL;
    
    public function generateToken($param) 
    {
        JWT::$leeway = 60;
        $result = array('status'=>'error','message'=>'Request has been Failed!');
        $time            = time();
        $notbefore_claim = $time + 600; //not before in seconds
        $expire_claim    = $time + 3600;
        if(count($param) > 0){
            $token = array(
              "iss" => $this->issuer_claim,
              "aud" => $this->audience_claim,
              "iat" => $time,
              "nbf" => $notbefore_claim,
              "exp" => $expire_claim,
              "data" => array(
                  "roletype"    => $param['roletype'],
                  "token"       => $param['token'],
                  "profile_img" => $param['profile_img'],
                  "fullname"    => $param['fullname']
            )); 
            $jwt    = JWT::encode($token, $this->secret_key);
            $result = array("message" => "Successfully loggedin!","token" => $jwt,"status" => 'success');
        }
        return $result;
    }
    
    public function getToken() 
    {
        JWT::$leeway = 60;
        $result = array('status'=>'error','message'=>'Request Failed!');
        $header = apache_request_headers();
        if(isset($header['Authorization']) && $header['Authorization']!='' && $header['Authorization']!=null){
            $arr = explode("Bearer ", $header['Authorization']);
            $jwt = $arr[1];
            if(isset($jwt) && $jwt!=''){
                try {
                    $decoded = JWT::decode($jwt, $this->secret_key, array('HS256'));
                    $result  = array("message" => "Access granted:",'status'=>'success','token'=>$decoded);              
                } catch (Exception $e){
                    $result  = array("message" => "Access denied:".$e->getMessage(),'status'=>'error');
                } 
            }
        }
        return json_encode($result,true);
    }
    
    public function decodeJwtToken($param) {
        $result = array('status'=>'error','message'=>'Request Failed!');
        $jwt = $param;
        if(isset($jwt) && $jwt!=''){
            try {
                $decoded = JWT::decode($jwt, $this->secret_key, array('HS256'));
                $result  = array("message" => "Access granted:",'status'=>'success','token'=>$decoded);              
            } catch (Exception $e){
                $result  = array("message" => "Access denied:".$e->getMessage(),'status'=>'error');
            } 
        }
        return json_encode($result,true);   
    }

    public function simpledecodeJwtToken($param) {
        $result = array('status'=>'error','message'=>'Request Failed!');
        $jwt = $param;
        if(isset($jwt) && $jwt!=''){
            try {
                $decoded = json_decode(base64_decode(str_replace('_', '/', str_replace('-','+',explode('.', $jwt)[1]))));
                $result  = array("message" => "Access granted:",'status'=>'success','token'=>$decoded);              
            } catch (Exception $e){
                $result  = array("message" => "Access denied:".$e->getMessage(),'status'=>'error');
            } 
        }
        return $result;   
    }

    
}
?>