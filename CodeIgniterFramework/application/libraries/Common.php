<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once __DIR__."/../../env.php";
class Common 
{
    private $CI;
    function __construct()
    {
        $this->CI = &get_instance();
    }

    function getmail_id($token)
    {
        $result   = array();
        $response = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v2/tokeninfo?id_token=' . $token), true);
        $aut      = $response["audience"];
        $ist      = $response["issued_to"];
        if(isset($response['verified_email']) && $response['verified_email']!=''){
            if (($aut == GSIGN_CLIENT_ID) && ($ist == GSIGN_CLIENT_ID)) {
                return $response;
            } else {
                return $result;
            }
        } else {
            return $result;
        }
    }

    function get_percentage_value($percentage,$total)
    {
        $result = ($percentage / 100) * $total;
        return $result; 
    }

    function number_to_roman_letter($number) 
    {
        $map    = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $result = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if($number >= $int) {
                    $number -= $int;
                    $result .= $roman;
                    break;
                }
            }
        }
        return $result;
    }

    function get_mime_type($filename) 
    {
        $idx           = explode('.', $filename);
        $count_explode = count($idx);
        $idx           = strtolower($idx[$count_explode-1]);
        $mimet = array( 
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            /* images*/
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            /* archives*/
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            /*audio/video*/
            'aac' => 'audio/aac',
            'mp3' => 'audio/mpeg',
            'qt'  => 'video/quicktime',
            'mov' => 'video/quicktime',
            'avi' =>'video/x-msvideo',
            'mp4' =>'video/mp4',
            'mpeg' =>'video/mpeg',
            'oga' =>'audio/ogg',
            'ogv' =>'video/ogg',
            'ts' =>'video/mp2t',
            'wav' =>'audio/wav',
            'weba' =>'audio/webm',
            'webm' =>'video/webm',
            '3gp' => 'video/3gpp',

            /*adobe*/
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            /* ms office*/
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',


            /* open office*/
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        if (isset($mimet[$idx])) {
            return $mimet[$idx];
        }  else {
            return 'application/octet-stream';
        }
    }

    function removeSpecialChar($str='')
    {
        $result = $str;
        if($str !=''){
            $result = trim($str, '`');
        }
        return $result;
    }
    
    function compressImage($source, $quality=70) 
    { 
        $imgInfo  = getimagesize($source); 
        $dataMine = $imgInfo['mime']; 
        switch($dataMine){ 
            case 'image/jpeg': 
                $image = imagecreatefromjpeg($source); 
                break; 
            case 'image/png': 
                $image = imagecreatefrompng($source); 
                break; 
            case 'image/gif': 
                $image = imagecreatefromgif($source); 
                break; 
            default: 
                $image = imagecreatefromjpeg($source); 
        }
        $width      = imagesx($image);
        $height     = imagesy($image);
        $thumbWidth = 500;
        $new_width  = 500;
        $new_height = floor($height * ($thumbWidth / $width));
        $tmp_img    = imagecreatetruecolor($new_width,$new_height);
        imagecopyresized($tmp_img,$image,0,0,0,0,$new_width,$new_height,$width,$height);
        ob_start(); 
            imagejpeg($tmp_img,null,$quality);
            $contents = ob_get_contents(); 
        ob_end_clean(); 
       return $contents; 
    }

    function get_spreadsheet_id($filename) 
    {
        $mimet = array( 
            'socialenterprise'       => $this->CI->config->item('INTERNSHIP_SHEET'),
            'Academic-Details'       => $this->CI->config->item('STUDENT_DATA_SHEET'),
            'donar'                  => $this->CI->config->item('DONAR_DATA_SHEET'),
            'StudentExtracurricular' => $this->CI->config->item('EXTRA_CURRICULAR'),
            'personal-details'       => $this->CI->config->item('STUDENT_DATA_SHEET'),
            'Donar-map'              => $this->CI->config->item('STUDENT_DATA_SHEET'),
            'Mentor-details'         => $this->CI->config->item('STUDENT_DATA_SHEET')
        );

        if (isset($mimet[$filename])) {
            return $mimet[$filename];
        }  else {
            return false;
        }
    }

    function get_spreadsheet_data($filename)
    {
        $result = array();
        if(isset($filename) && $filename!=''){
            $spreadsheet_id = $this->get_spreadsheet_id($filename);
            if($spreadsheet_id){
                $url            = sprintf('https://sheets.googleapis.com/v4/spreadsheets/'.$spreadsheet_id.'/values/'.$filename.'?key=%s', $this->CI->config->item('SPREADSHEET_API_KEY'));
                $json   = json_decode(file_get_contents($url),true);
                $result = $json['values'];
            }
        }
        return $result;
    }

    function removeWhiteSpaces($string)
    {
        return trim(preg_replace('/[\t\n\r\s]+/', ' ', $string));
    }


    function moneyformat($num)
    {
        $explrestunits = "" ;
        if(strlen($num)>3) {
            $lastthree = substr($num, strlen($num)-3, strlen($num));
            $restunits = substr($num, 0, strlen($num)-3); 
            $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits;
            $expunit = str_split($restunits, 2);
            for($i=0; $i<sizeof($expunit); $i++) {
                if($i==0) {
                    $explrestunits .= (int)$expunit[$i].",";
                } else {
                    $explrestunits .= $expunit[$i].",";
                }
            }
            $thecash = $explrestunits.$lastthree;
        } else {
            $thecash = $num;
        }
        return $thecash;
    }
    
    function get_client_ip()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
                $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
                $ip = $forward;
        } else {
                $ip = $remote;
        }

        return $ip;
    }

    function differencechecker($old, $new)
    {
        $from_start = strspn($old ^ $new, "\0");        
        $from_end   = strspn(strrev($old) ^ strrev($new), "\0");

        $old_end    = strlen($old) - $from_end;
        $new_end    = strlen($new) - $from_end;

        $start      = substr($new, 0, $from_start);
        $end        = substr($new, $new_end);
        $new_diff   = substr($new, $from_start, $new_end - $from_start);  
        $old_diff   = substr($old, $from_start, $old_end - $from_start);

        $new        = "$start<ins style='background-color:#ccffcc'>$new_diff</ins>$end";
        $old        = "$start<del style='background-color:#ffcccc'>$old_diff</del>$end";
        return array("old"=>$old, "new"=>$new);
    }

    function isJSON($string){
       return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

    function seo_title_generate($title) {
        $seotitle = preg_replace("/[^a-zA-Z0-9\s]/","",$title);
        $seotitle = trim($seotitle);
        $seotitle = preg_replace("/\s+/"," ",$seotitle);
        $seotitle = str_replace(" ","-",$seotitle);
        $seotitle = strtolower($seotitle);
        return $seotitle;
    }

    function json_validate($string='')
    {
        if($string!=''){
            // decode the JSON data
            $result = json_decode($string,true);

            // switch and check possible JSON errors
            switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    $error = ''; // JSON is valid // No error has occurred
                    break;
                case JSON_ERROR_DEPTH:
                    $error = 'The maximum stack depth has been exceeded.';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    $error = 'Invalid or malformed JSON.';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    $error = 'Control character error, possibly incorrectly encoded.';
                    break;
                case JSON_ERROR_SYNTAX:
                    $error = 'Syntax error, malformed JSON.';
                    break;
                // PHP >= 5.3.3
                case JSON_ERROR_UTF8:
                    $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                    break;
                // PHP >= 5.5.0
                case JSON_ERROR_RECURSION:
                    $error = 'One or more recursive references in the value to be encoded.';
                    break;
                // PHP >= 5.5.0
                case JSON_ERROR_INF_OR_NAN:
                    $error = 'One or more NAN or INF values in the value to be encoded.';
                    break;
                case JSON_ERROR_UNSUPPORTED_TYPE:
                    $error = 'A value of a type that cannot be encoded was given.';
                    break;
                default:
                    $error = 'Unknown JSON error occured.';
                    break;
            }

            if ($error !== '') {
                // throw the Exception or exit // or whatever :)
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
        

        // everything is OK
        //return $result;
    }

    function upload_drive($key,$URL)
    {
        $file_tmpname = $key;
        $data1        = base64_encode(file_get_contents($file_tmpname));
        $type         = mime_content_type($key);
        // die($type);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $data = array(
            'file' => $data1,
            'ftype' => $type
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
       $output = curl_exec($ch);
        curl_close($ch);
        echo "<pre>"; print_r($output); echo "<pre>";
        $output = json_decode($output, true);
        echo "<pre>"; print_r($output); echo "<pre>";
        if (isset($output['id'])) {
            return $output['id'];
        }
    }
}
?>