<?php

header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Headers: *'); 
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); 
date_default_timezone_set('Asia/Kolkata');
//error_reporting(0);
error_reporting(E_ALL);
class Api extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('UserModel');
        $this->load->model('EventModel');
        $this->load->model('MasterModel');
        $this->load->model('Menus_model');
        /*$this->load->model('AcademicModel');*/
        $this->load->model('LogModel');
        $this->load->helper('url','form');
        $this->load->library(array('common','CommonEmail','EncryptionFunction','JWT_Token','session','email'));
    }

    public function index() {

    }

    public function getTokenData() 
    {
        $get_token   = json_decode($this->jwt_token->getToken(),true);
        $result['message'] = 'Something went Wrong No actions Done!';
        $result['status']  = 'error';              
        if(isset($get_token['status']) && $get_token['status']=='success'){
            $token  = $get_token['token']['data']['token'];
            if(isset($token) && $token!=''){
                $postdata = array('token'=>$token);
                $result   = $this->verifyUserDetails($postdata);
            }
        }
        return $result;
    }

    public function verifyTokenData() 
    {
        $get_token   = json_decode($this->jwt_token->getToken(),true);
        $activity    = $this->validateToken();
        echo json_encode($get_token,true);
    }

    public function validateToken($postdata=array())
    {
        $response['status']  = 'error';
        $response['message'] = 'Invalid request!';
        if(count($postdata) ==0){
            $postdata            = $this->getTokenData();
        }
        if(isset($postdata['status']) && $postdata['status'] =='success'){
            $token = $postdata['data']; 
            if(isset($token['id']) && $token['id']!=''){
                $users_id = $token['id'];
                $data  = array('id'=>$users_id,'status'=>'1','select'=>"id,role");
                $query = $this->UserModel->get_users($data);
                $day1  = date('Y-m-d H:i:s');
                if(count($query) > 0){
                    $user_activies                      = array();
                    $user_activies['users_id']     = $users_id; 
                    $user_activies['is_web']            = 1;
                    $user_activies['ip_web']            = $this->common->get_client_ip();
                    $user_activies['web_current_date']  = $day1;
                    $user_activies['created']           = $day1;
                    $user_activies['modified']          = $day1;
                    $admin_activies_data                = $this->UserModel->users_activies_data($user_activies, 'web');
                    $response['status']   = 'success';
                    $response['message']  = 'User Logged In!';
                } else {
                    $response['status']   = 'error';
                    $response['message']  = 'Not User Logged In!';
                }
            }
        }  
        return $response;
    }

    public function extend_user(){
        $response['status']  = 'error';
        $response['message'] = 'Invalid request!';
        $postdata            = $this->getTokenData();
        if(isset($postdata) && count($postdata) > 0){
            $result = $postdata['data'];    
            $email  = $result['email'];
            $data   = array('email'=>$email,'status'=>'1','roles_id'=>'2','select'=>"id,role,name,email");
            $query  = $this->UserModel->get_users($data);
            if(count($query) > 0) {
                $role_string = 'user';
                
                $user_gender  = 'Male';
                $gender       = $this->config->item('gender');
                $user_profile = base_url().$this->config->item('student_photo').$user_gender.'.png';


                $query[0]['users_id']  = $this->encryptionfunction->aes_encrypt_string($query[0]['id']);
                $query[0]['auth_time']      = $this->encryptionfunction->aes_encrypt_string(date('Y-m-d H:i:s'));
                $res['token']               = $this->encryptionfunction->aes_encrypt_string(serialize($query[0]));
                $res['roletype']            = $role_string;
                $res['profile_img']         = $user_profile;
                $res['fullname']            = $query[0]['fullname'];
                $response                   = $this->jwt_token->generateToken($res);
                $result                     = array('status'=>'success','data'=>array('id'=>$query[0]['id']));
                $this->validateToken($result);
            } else {
                $response['status']      = 'Error';
                $response['message']     = 'User Details Not found! Please Contact Administrator';
            }


        }
        echo json_encode($response); exit; 
    }

    public function login() 
    {
        header('Content-Type: application/json');
        $postdata            = json_decode(file_get_contents("php://input"), TRUE);
        $response['status']  = 'error';
        $response['message'] = 'Invalid request!';
        if(isset($postdata) && count($postdata) > 0){
            if(isset($postdata['socialLogin']) && $postdata['socialLogin'] !=''){ 
                //$jwt_token = $this->jwt_token->decodeJwtToken($postdata['gtoken']);
                $result    = $this->common->getmail_id($postdata['gtoken']);
                if(count($result) > 0){
                    $email  = $result['email'];
                    $data   = array('email'=>$email,'status'=>'1','select'=>"id,role,name,email");
                    if(isset($postdata['role']) && $postdata['role']!=''){
                        $data['role']= $postdata['role'];
                    }

                    if(isset($postdata['source']) && $postdata['source']=='weblogin'){
                        $data['role_not']= '0';
                    }
                    
                    $query  = $this->UserModel->get_users($data);
                    if(count($query) > 0) {
                        $role_string = 'faculty';
                        /*$data        = array('id'=>$query[0]['roles_id'],'select'=>"role");
                        $get_roles   = $this->MasterModel->get_roles($data);
                        if(count($get_roles) > 0){
                            $role_string   = strtolower($get_roles[0]['role']);
                        }*/

                        $user_gender  = 'Male';
                        $gender       = $this->config->item('gender');
                        $user_profile = base_url().$this->config->item('student_photo').$user_gender.'.jpg';

                        /*$cond         = array('users_id'=>$query[0]['id'],'select'=>"gender");
                        $personalDe   = $this->UserModel->get_users_personal_details($cond);
                        if(count($personalDe) > 0){
                            if(count($gender) > 0){
                                foreach ($gender as $keys => $gen_value) {
                                    if($keys == $personalDe[0]['gender']){
                                        $user_gender = $gen_value;
                                    }
                                }
                            }
                        }*/

                        /*$cond     = array('users_id'=>$query[0]['id'],'document_types_id'=>'1','select'=>"file_type_id,filename");
                        $document = $this->UserModel->get_users_educational_documents($cond);
                        if(count($document) > 0){
                            $user_profile = 'data:'.$document[0]['file_type_id'].';base64,'.base64_encode($document[0]['filename']);
                        }*/

                        $query[0]['users_id']  = $this->encryptionfunction->aes_encrypt_string($query[0]['id']);
                        $query[0]['auth_time']      = $this->encryptionfunction->aes_encrypt_string(date('Y-m-d H:i:s'));
                        $res['token']               = $this->encryptionfunction->aes_encrypt_string(serialize($query[0]));
                        $res['roletype']            = $role_string;
                        $res['profile_img']         = $user_profile;
                        $res['fullname']            = $query[0]['name'];
                        $res['jwt_token']           = $postdata['gtoken'];
                        $response                   = $this->jwt_token->generateToken($res);
                        $result                     = array('status'=>'success','data'=>array('id'=>$query[0]['id']));
                        $this->validateToken($result);
                        if(isset($response['status']) && $response['status']=='success'){
                            $result['token']   = $postdata['gtoken'];
                            $response['data'] =  $this->sampledecode($result);
                        }
                    } else {
                        $response['status']      = 'Error';
                        $response['message']     = 'User Details Not found! Please Contact Administrator';
                    }
                } else {
                    $response['status']      = 'Error';
                    $response['message']     = 'Invalid request!';
                }
            }
        }
        echo json_encode($response); exit;
    }

    public function sampledecode($param) 
{
    $activity  = $this->jwt_token->simpledecodeJwtToken($param['token']);
    $data      = array();
    if(isset($activity['status']) && $activity['status']=='success'){
        $tok  = $activity['token'];
        $data = array("name"=>$tok->name,"picture"=>$tok->picture,"given_name"=>$tok->given_name,"family_name"=>$tok->family_name);
    }
    return $data;
}

    public function validateUser() 
    {
        header('Content-Type: application/json');
        $postdata  = json_decode(file_get_contents("php://input"), TRUE);
        $response['status']='error';
        $response['message']='Invalid request!';
        if(isset($postdata) && count($postdata) > 0){
            if(isset($postdata['email']) && $postdata['email']!=''){
                $data  = array('email'=>$postdata['email'],'status'=>'1','oauth_uid'=>$postdata['oauth_uid']);
                $query = $this->UserModel->get_users($data);
            } else if(isset($postdata['mobile']) && $postdata['mobile']!=''){
                $data  = array('mobile'=>$postdata['mobile'],'status'=>'1');
                $query = $this->UserModel->get_users($data);
            }
            if(isset($query) && count($query) > 0){
                $response['status']  = 'error';
                $response['message'] = 'User Already Exist!'; 
            } else {
                $response['status']  = 'success';
                $response['message'] = 'New User!';
            }
        }
        echo json_encode($response); 
    }

    public function verifyToken()
    {
        $result       = $this->getTokenData();
        if(isset($result['status']) && $result['status'] =='success'){
            $response = true;
        } else {
            $response = false;
        }
        echo $response;exit;
    }


    public function verifyUserDetails($postdata=array())
    {
        $response['status']='error';
        $response['message']='Invalid request!';       
        if(isset($postdata) && count($postdata) > 0){
            $token = unserialize($this->encryptionfunction->aes_decrypt_string($postdata['token'])); 
            if(isset($token['users_id']) && $token['users_id']!=''){
                $data  = array('id'=>$this->encryptionfunction->aes_decrypt_string($token['users_id']),'status'=>'1','select'=>"id,email");
                $query = $this->UserModel->get_users($data);
                $day1      = date('Y-m-d H:i:s');
                $day1      = strtotime($day1);
                $day2      = $this->encryptionfunction->aes_decrypt_string($token['auth_time']);
                $day2      = strtotime($day2);
                $diffHours = round(($day1 - $day2) / 3600);
                if($diffHours < 24){
                    if(count($query) > 0){
                        $response['data']    = $query[0];
                        $response['status']  = 'success';
                        $response['message'] = 'User Logged In!';
                    } else {
                        $response['status']  = 'error';
                        $response['message'] = 'Not User Logged In!';
                    }
                } else {
                    $response['status']  = 'error';
                    $response['message'] = 'Not User Logged In!';
                }
            }
        }  
        return $response;
    }


    function getEvents($id=''){
        header('Content-Type: application/json');
        $postdata  = json_decode(file_get_contents("php://input"), TRUE);
        $todayDate = date('Y-m-d');
        $response  = array('status'=>'error','message'=>"No Record Found!");
        if(isset($postdata['source'])){
            if($postdata['source'] == 'eventspage'){
                $result = array('status'=>"success",'data'=>array('id'=>'web'));
                $isweb  = true;
            } else if($postdata['source'] == 'eventspage-reg'){
                $result = $this->getTokenData();
                $isweb  = false;
            }
            if(isset($result['status']) && $result['status']=='success'){
                $logID = $result['data']['id'];
                $data       = array('status'=>'1');
                $events_all = array();
                if(isset($postdata['seo_url']) && $postdata['seo_url']!=''){
                    $data['seo_title'] = $postdata['seo_url'];
                }

                if(isset($id) && $id!=''){
                    $data['id'] = $this->encryptionfunction->aes_decrypt_string($id);
                }

                if(isset($postdata['school']) && $postdata['school']!=''){
                    $data['event_school'] = $postdata['school'];
                }

                if($postdata['source'] == 'eventspage-reg'){
                    $data['order_by']  = 'id';
                } 

                if(isset($postdata['select']) && $postdata['select']!=''){
                    $data['select'] = $postdata['select'];
                }

                //image,
                $data['select'] = "id,event_title,seo_title,image,image_type,timeslot_id,timeslots,department,event_type,description,contact,status,is_deleted,created_date,registration,eventcategory,event_school,is_link";
                if($postdata['source'] == 'eventspage'){
                    $data['keyword']      = $todayDate;
                    $data['is_duplicate'] = '1';
                    $events_all       = $this->EventModel->get_events($data);
                    unset($data['keyword']);
                }
                $events         = $this->EventModel->get_events($data);
                $events         = array_merge($events_all,$events);
                if(count($events) > 0){
                    $inc_no = 1;
                    foreach ($events as $key => $value) {
                        if($value['event_type'] == '1'){
                            $event_type   = 'Virtual';
                        } else if($value['event_type'] == '2'){
                            $event_type   = 'Physical';
                        } else {
                            $event_type   = 'Both Physical and Virtual';
                        }

                        $events[$key]['title']       = $value['event_title'];
                        $events[$key]['seo_title']   = $value['seo_title'];
                        if($isweb || $id!=''){
                            //$events[$key]['image']       = $value['image_type'].','.base64_encode($value['image']);
                            $events[$key]['image']       = $this->config->item('viewDriveImage').$value['image'];
                            
                            $events[$key]['description'] = $value['description'];
                        } else {
                            $events[$key]['image']       = '';
                            $events[$key]['description'] = '';
                        } 

                        $events[$key]['id']             = $this->encryptionfunction->aes_encrypt_string($value['id']);
                        $events[$key]['department']     = $value['department']; 
                        $events[$key]['event_type']     = $value['event_type'];
                        $events[$key]['event_ty']       = $event_type;
                        $events[$key]['registration']   = $value['registration'];
                        $events[$key]['timeslot']       = json_decode($value['timeslots'],true);
                        $events[$key]['contact']        = json_decode($value['contact'],true);
                        $events[$key]['event_timeslot'] = $value['timeslot_id'];
                        $events[$key]['eventcat']       = $value['eventcategory'] == '1'?'General':'Webinar';
                        $events[$key]['link']           = $value['is_link'] == '1'?true:false;

                        $event_school = '';
                        $cond         = array('id'=>$value['event_school']);
                        $resNot       = $this->EventModel->get_event_category($cond);
                        if(count($resNot) > 0){
                            $event_school = $resNot[0]['name'];
                        }

                        $events[$key]['school']         = $event_school;
                        $condi         = array('status'=>'1');
                        $resNots       = $this->EventModel->get_event_category($condi);
                        if(count($resNots) > 0){
                            foreach($resNots as $resKey => $resVal){
                                if($resVal['id'] == $value['event_school']){
                                    $resNots[$resKey]['selected'] = true;
                                } else {
                                    $resNots[$resKey]['selected'] = false;
                                }
                            }
                        }

                        $events[$key]['event_school']   = $value['event_school'];
                        $events[$key]['event_cat']      = $resNots;
                        $events[$key]['eventcategory']  = $value['eventcategory'];
                        $events[$key]['created']        = date('F j,Y',strtotime($value['created_date']));
                        $events[$key]['inc_no']         = $inc_no;
                        $inc_no++;
                    }

                    if($postdata['source'] == 'eventspage'){
                        if(count($events) > 0){
                            $result_ev = array();
                            foreach ($events as $key => $value) {
                                $res = array();
                                if(isset($value['timeslot']) && count($value['timeslot']) >0){
                                    $timeslot = $value['timeslot'];
                                    for($i = 0; $i<=count($timeslot);$i++){
                                        if(isset($timeslot[$i])){
                                            $value_timeslot      = $timeslot[$i];
                                            $res['year']         = date('Y',strtotime($value_timeslot['date']));
                                            $res['month']        = date('n',strtotime($value_timeslot['date'])); 
                                            $res['day']          = date('j',strtotime($value_timeslot['date'])); 
                                            if(isset($postdata['isview']) && $postdata['isview'] == true){
                                                $res['description']  = $value['description'];
                                                $res['contact']      = $value['contact'];
                                            } else {
                                                $res['description']  = substr($value['description'], 0, 230);
                                            }
                                            $res['timeslot']      = json_decode($value['timeslots'],true);
                                            $res['event_ty']      = $value['event_ty'];
                                            $res['event_date']    = date('F j,Y',strtotime($value_timeslot['date']));
                                            $res['group_date']    = $value_timeslot['date'];
                                            $res['event']         = $value['id'];
                                            $res['school']        = $value['school'];
                                            $res['image']         = $value['image'];
                                            $res['title']         = $value['event_title'];
                                            $res['registration']  = $value['registration'];
                                            $res['eventcategory'] = $value['eventcat'];
                                            $res['category']      = $value['eventcategory'];
                                            $res['seo_title']     = $value['seo_title'];
                                            $res['department']    = $value['department'];
                                            $result_ev[]         = $res;
                                        }
                                    }
                                }
                            }
                        //$result_ev = array_merge($result_ev,$result_ev);
                        }

                        $response['status']     = 'success';
                        $response['message']    = 'Successfully Events Retrived!';
                        $response['events']     = $result_ev;
                    } else {
                        $response['status']     = 'success';
                        $response['message']    = 'Successfully Events Retrived!';
                        $response['data']       = $events;
                    }
                }
            } else {
                $response['status']  = 'logout';
                $response['message'] = 'Invalid User Details';
            }
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Invalid Source Details';
        }
        echo json_encode($response);exit;
    }

    function getEventsweb($id=''){
        header('Content-Type: application/json');
        $postdata  = json_decode(file_get_contents("php://input"), TRUE);
        $todayDate = date('Y-m-d');
        $response  = array('status'=>'error','message'=>"No Record Found!");
        if(isset($postdata['source'])){
            if($postdata['source'] == 'eventspage'){
                $result = array('status'=>"success",'data'=>array('id'=>'web'));
                $isweb  = true;
            } else if($postdata['source'] == 'eventspage-reg'){
                $result = $this->getTokenData();
                $isweb  = false;
            }
            if(isset($result['status']) && $result['status']=='success'){
                $logID = $result['data']['id'];
                $data       = array('status'=>'1','is_duplicate'=>'1');
                $events_all = array();
                if(isset($postdata['seo_url']) && $postdata['seo_url']!=''){
                    $data['seo_title'] = $postdata['seo_url'];
                }

                if(isset($id) && $id!=''){
                    $data['id'] = $this->encryptionfunction->aes_decrypt_string($id);
                }

                if(isset($postdata['events_id']) && $postdata['events_id']!=''){
                    $data['events_id'] = $this->encryptionfunction->aes_decrypt_string($postdata['events_id']);
                }

                //image,

                if(isset($postdata['date'])){
                    $data['date']  = $postdata['date'];

                }

                if(isset($postdata['select'])){
                    $data['select']  = $postdata['select'];

                }

                if(isset($postdata['group_date'])){
                    $data['group_date']  = $postdata['date_check'];
                }

                if(isset($postdata['date_check'])){
                    $data['date_check']  = $postdata['date_check'];
                }

                $events         = $this->EventModel->get_events_timeslot($data);
                if(count($events) > 0){
                    foreach ($events as $key => $val) {
                        $data   = array('id'=>$val['events_id']);
                        $data['select'] = "id,event_title,seo_title,image,image_type,timeslot_id,timeslots,department,event_type,description,contact,status,is_deleted,created_date,registration,eventcategory,event_school";
                        $result = $this->EventModel->get_events($data);
                        if(count($result) > 0){
                            $value = $result[0];
                            if($value['event_type'] == '1'){
                                $event_type   = 'Virtual';
                            } else if($value['event_type'] == '2'){
                                $event_type   = 'Physical';
                            } else {
                                $event_type   = 'Both Physical and Virtual';
                            }

                            $events[$key]['title']       = $value['event_title'];
                            $events[$key]['seo_title']   = $value['seo_title'];
                            if($isweb || $id!=''){
                                $events[$key]['description'] = substr($value['description'], 0, 230);
                            } else {
                                $events[$key]['description'] = $value['description'];
                            } 
                            $events[$key]['image']          = $value['image_type'].','.base64_encode($value['image']);
                            $events[$key]['id']             = $this->encryptionfunction->aes_encrypt_string($value['id']);
                            $events[$key]['department']     = $value['department']; 
                            $events[$key]['event_type']     = $value['event_type'];
                            $events[$key]['event_ty']       = $event_type;
                            $events[$key]['registration']   = $value['registration'];
                            $events[$key]['timeslot']       = json_decode($value['timeslots'],true);
                            $events[$key]['contact']        = json_decode($value['contact'],true);
                            $events[$key]['event_timeslot'] = $value['timeslot_id'];
                            $events[$key]['eventcat']       = $value['eventcategory'] == '1'?'General':'Webinar';
                            $event_school = '';
                            if(isset($value['event_school']) && $value['event_school']!=''){
                                $event_school = '';
                                $cond         = array('id'=>$value['event_school']);
                                $resNot       = $this->EventModel->get_event_category($cond);
                                if(count($resNot) > 0){
                                    $event_school = $resNot[0]['name'];
                                }
                            }
                            $events[$key]['school']         = $event_school;
                            $events[$key]['event_school']   = $value['event_school'];
                            $events[$key]['eventcategory']  = $value['eventcategory'];
                            $events[$key]['event_date']     = date('F j,Y',strtotime($val['date']));
                            $events[$key]['group_date']     = $val['date'];
                        }
                    }

                    $response['status']     = 'success';
                    $response['message']    = 'Successfully Events Retrived!';
                    $response['data']       = $events;
                }
            } else {
                $response['status']  = 'logout';
                $response['message'] = 'Invalid User Details';
            }
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Invalid Source Details';
        }
        echo json_encode($response);exit;
    }

    function duplicateEvents($id){
        header('Content-Type: application/json');
        $result   = $this->getTokenData();
        $jsonData = json_decode(file_get_contents("php://input"), TRUE);
        $response = array('status'=>'error','message'=>"No Record Found!");
        if(isset($result['status']) && $result['status'] =='success'){
            $logID          = $result['data']['id'];
            if($id!=''){
                $id       = $this->encryptionfunction->aes_decrypt_string($id);
                $data     = array('id'=>$id,'status'=>'1');
                $details  = $this->EventModel->get_events($data);
                if(count($details) >0){
                    if(isset($jsonData['duplicateTimes']) && $jsonData['duplicateTimes']!=''){
                        $length = (int)$jsonData['duplicateTimes'];

                        for($i=0;$i<=$length;$i++ ){
                            $postdata               = $details[0];
                            $data                   = array();
                            $data['event_title']    = $postdata['event_title'];
                            $data['department']     = $postdata['department'];
                            $data['seo_title']      = '';
                            $data['event_type']     = $postdata['event_type'];
                            $data['timeslot_id']    = $postdata['timeslot_id'];
                            $data['description']    = $postdata['description'];
                            $data['timeslots']      = $postdata['timeslots'];
                            $data['contact']        = $postdata['contact'];
                            $data['image']          = $postdata['image'];
                            $data['eventcategory']  = $postdata['eventcategory'];
                            $data['image_type']     = $postdata['image_type'];
                            $data['is_duplicate']   = '2';
                            $data['created_by']     = $logID;
                            $data['modified_by']    = $logID;
                            $data['registration']   = $postdata['registration'];
                            $data['event_school']   = $postdata['event_school'];
                            $data['created_date']   = date('Y-m-d H:i:s');
                            $data['modified_date']  = date('Y-m-d H:i:s');
                            $insert                 = $this->EventModel->insert_events($data);
                            $action_name            = 'Duplicate';
                            $comment                = 'Duplicate Event and Create';
                            $id                     = $insert['id'];
                            if($insert){
                                $timeslots = json_decode($postdata['timeslots'],true);
                                if(count($timeslots) > 0){
                                    foreach ($postdata['timeslots'] as $key => $value) {
                                        $data                   = array();
                                        $data['events_id'] = $id;
                                        $data['date']           = $value['date'];
                                        $data['time']           = $value['time'];
                                        $data['modified_date']  = date('Y-m-d H:i:s');
                                        $data['modified_by']    = $logID;
                                        $data['created_by']     = $logID;
                                        $data['created_date']   = date('Y-m-d H:i:s');
                                        $this->EventModel->insert_events_timeslot($data);
                                    }
                                }

                                $data                   = array();
                                $data['events_id'] = $id;
                                $data['action']         = $action_name;
                                $data['comment']        = $comment;
                                $data['created_by']     = $logID;
                                $data['modified_by']    = $logID;
                                $data['created_date']   = date('Y-m-d H:i:s');
                                $data['modified_date']  = date('Y-m-d H:i:s');
                                $insert                 = $this->LogModel->insert_events_log($data);

                                $response['status']     = 'success';
                                $response['message']    = 'Successfully Event Duplicated!';
                            } else {
                                $response['status']     = 'error';
                                $response['message']    = 'Not Saved!';
                            }
                        }
                    }
                }
            } 
        } else {
            $response['status']  = 'logout';
            $response['message'] = 'Invalid User Details';
        }
        echo json_encode($response);exit;
    }


    function saveEvents($id=''){
        header('Content-Type: application/json');
        $postdata = json_decode(file_get_contents("php://input"), TRUE);
        $result   = $this->getTokenData();
        $response = array('status'=>'error','message'=>"No Record Found!");
        if(isset($result['status']) && $result['status'] =='success'){
            $imageName      = '';
            $logID          = $result['data']['id'];
            $image          = explode(",",$postdata['image']);
            $replacedTitle  = explode(",",$postdata['title']);
            $Title          = str_replace(array(',','.'),"",trim($postdata['title']));
            $folderPath     = FCPATH."assets/temp_uploads/events/";
            if (!is_dir($folderPath)) {
                mkdir($folderPath, 0777, true);
            }
            $image_parts    = explode(";base64,", $postdata['image']);
            $image_type_aux = explode($folderPath, $image_parts[0]);
            $image_base64   = base64_decode($image_parts[1]);
            $name           = uniqid() . '.png';
            $file           = $folderPath . $name;
            file_put_contents($file, $image_base64);
            $uploadurl     = $this->config->item('driveUploadURL');
            $isImageUpload = $this->common->upload_drive($file,$uploadurl);
            echo "Image Id".$isImageUpload;exit;
            if($isImageUpload){
                $imageName = $isImageUpload;
                echo $isImageUpload;
                echo "coming";exit;
                unlink($file);
            } 
            if($id!='' && $id!='add'){
                $id       = $this->encryptionfunction->aes_decrypt_string($id);
                $data     = array('id'=>$id,'status'=>'1');
                $details  = $this->EventModel->get_events($data);
                if(count($details) >0){
                    $data     = array(); $condition = array('id'=>$details[0]['id']);
                    $data['event_title']    = $postdata['title'];
                    $data['department']     = $postdata['department'];
                    $data['seo_title']      = $this->common->seo_title_generate($replacedTitle[0]);
                    $data['event_type']     = $postdata['event_type'];
                    $data['timeslot_id']    = $postdata['event_timeslot'];
                    $data['description']    = $postdata['description'];
                    $data['timeslots']      = json_encode($postdata['timeslot'],true);
                    $data['contact']        = json_encode($postdata['contact'],true);
                    $data['eventcategory']  = $postdata['eventcategory'];
                    $data['image']          = $imageName;
                    $data['is_duplicate']   = '1';
                    $data['image_type']     = $image[0];
                    $data['is_link']        = $postdata['is_link'];
                    $data['registration']   = $postdata['registration'];
                    $data['event_school']   = $postdata['event_school'];
                    $data['modified_by']    = $logID;
                    $data['modified_date']  = date('Y-m-d H:i:s');
                    $insert                 = $this->EventModel->update_events($data,$condition);
                    $action_name            = 'Update';
                    $comment                = 'Event Updated';

                }
            } else {
                $data                   = array();
                $data['event_title']    = $postdata['title'];
                $data['department']     = $postdata['department'];
                $data['seo_title']      = $this->common->seo_title_generate($replacedTitle[0]);
                $data['event_type']     = $postdata['event_type'];
                $data['timeslot_id']    = $postdata['event_timeslot'];
                $data['description']    = $postdata['description'];
                $data['timeslots']      = json_encode($postdata['timeslot'],true);
                $data['contact']        = json_encode($postdata['contact'],true);
                $data['image']          = $imageName;
                $data['eventcategory']  = $postdata['eventcategory'];
                $data['image_type']     = $image[0];
                $data['created_by']     = $logID;
                $data['modified_by']    = $logID;
                $data['is_duplicate']   = '1';
                $data['is_link']        = $postdata['is_link'];
                $data['registration']   = $postdata['registration'];
                $data['event_school']   = $postdata['event_school'];
                $data['created_date']   = date('Y-m-d H:i:s');
                $data['modified_date']  = date('Y-m-d H:i:s');
                $insert                 = $this->EventModel->insert_events($data);
                $action_name            = 'Insert';
                $comment                = 'Event Inserted';
                $id                     = $insert['id'];
            }
            unlink($file);
            if($insert){
                $condition = array('events_id'=>$id);
                $select    = $this->EventModel->get_events_timeslot($condition);
                if(count($select) > 0){
                    $this->EventModel->delete_events_timeslot($condition);
                } 

                if(count($postdata['timeslot']) > 0){
                    foreach ($postdata['timeslot'] as $key => $value) {
                        $data                   = array();
                        $data['events_id'] = $id;
                        $data['date']           = $value['date'];
                        $data['time']           = $value['time'];
                        $data['modified_date']  = date('Y-m-d H:i:s');
                        $data['modified_by']    = $logID;
                        $data['created_by']     = $logID;
                        $data['created_date']   = date('Y-m-d H:i:s');
                        $this->EventModel->insert_events_timeslot($data);
                    }
                }


                $data                   = array();
                $data['events_id'] = $id;
                $data['action']         = $action_name;
                if($action_name=='Update'){
                   $image                 = $details[0]['image'];
                   $details[0]['image']    = '';

                   $data['image']          = $image;
                   $details                = json_encode($details, true);
                   $data['old']            = $details;
               }
               $data['comment']        = $comment;
               $data['created_by']     = $logID;
               $data['modified_by']    = $logID;
               $data['created_date']   = date('Y-m-d H:i:s');
               $data['modified_date']  = date('Y-m-d H:i:s');
               $insert                 = $this->LogModel->insert_events_log($data);

               if(isset($postdata['event_school']) && $postdata['event_school']!=''){
                $event_school = '';
                $cond         = array('id'=>$postdata['event_school']);
                $resNot       = $this->EventModel->get_event_category($cond);
                if(count($resNot) > 0){

                    $event_school = $resNot[0]['name'];
                }
            }
            $response['status']     = 'success';
            $response['message']    = 'Successfully Event Action Done!';
        } else {
            $response['status']     = 'error';
            $response['message']    = 'Not Saved!';
        }


    } else {
        $response['status']  = 'logout';
        $response['message'] = 'Invalid User Details';
    }
    echo json_encode($response);exit;
}

function deleteEvents($id){
    header('Content-Type: application/json');
    $result   = $this->getTokenData();
    $response = array('status'=>'error','message'=>"No Record Found!");
    if(isset($result['status']) && $result['status'] =='success'){
        $logID          = $result['data']['id'];
        if($id!=''){
            $id       = $this->encryptionfunction->aes_decrypt_string($id);
            $data     = array('id'=>$id,'status'=>'1');
            $details  = $this->EventModel->get_events($data);
            if(count($details) >0){
                $data     = array(); $condition = array('id'=>$details[0]['id']);
                $data['is_deleted']     = '1';
                $data['modified_by']    = $logID;
                $data['modified_date']  = date('Y-m-d H:i:s');
                $delete                 = $this->EventModel->update_events($data,$condition);
                if($delete){
                    $condition = array('events_id'=>$details[0]['id']);
                    $select    = $this->EventModel->get_events_timeslot($condition);
                    if(count($select) > 0){
                        $this->EventModel->delete_events_timeslot($condition);
                    } 

                    $image                  = $details[0]['image'];
                    $details[0]['image']    = '';
                    $data                   = array();
                    $data['events_id'] = $details[0]['id'];
                    $data['action']         = 'Delete';
                    $data['image']          = $image;
                    $details                = json_encode($details, true);
                    $data['old']            = $details;
                    $data['comment']        = 'Event Deleted';
                    $data['created_by']     = $logID;
                    $data['modified_by']    = $logID;
                    $data['created_date']   = date('Y-m-d H:i:s');
                    $data['modified_date']  = date('Y-m-d H:i:s');
                    $insert                 = $this->LogModel->insert_events_log($data);

                    $response['status']     = 'success';
                    $response['message']    = 'Successfully Event Deleted!';
                } else {
                    $response['status']     = 'error';
                    $response['message']    = 'Not Saved!';
                }
            }
        } 
    } else {
        $response['status']  = 'logout';
        $response['message'] = 'Invalid User Details';
    }
    echo json_encode($response);exit;
}




function getUserLogs($id=''){
    header('Content-Type: application/json');
    $result   = $this->getTokenData();
    $response = array('status'=>'error','message'=>"No Record Found!");
    if(isset($result['status']) && $result['status'] =='success'){
        $logID     = $result['data']['id'];
        $data     = array();
        if($id!=''){
            $id                    = $this->encryptionfunction->aes_decrypt_string($id);
            $data['users_id'] = $id;
        }

        $data['select'] = "id,users_id,action,comment,created_date,created_by";
        $details        = $this->LogModel->get_users_log($data);
        if(count($details) >0){
            foreach ($details as $key => $value) {
                $data = array('id'=>$value['created_by']);
                $res  = $this->UserModel->get_users($data);
                if(count($res) > 0){
                 $details[$key]['created_by'] = $res[0]['name']; 
             }
             $details[$key]['created_date']     = date('F j,Y',strtotime($value['created_date']));
         }

         $response['status']  = 'success';
         $response['message'] = 'Successfully Deleted!';
         $response['data']    = $details;
     }
 } else {
    $response['status']  = 'logout';
    $response['message'] = 'Invalid User Details';
}
echo json_encode($response);exit;
}

function getEventNotification($id=''){
        header('Content-Type: application/json');
        $postdata  = json_decode(file_get_contents("php://input"), TRUE);
        $todayDate = date('Y-m-d');
        $response  = array('status'=>'error','message'=>"No Record Found!");
        $result    = $this->getTokenData();
        if(isset($result['status']) && $result['status']=='success'){
            $logID = $result['data']['id'];
            $data       = array('status'=>'1');
            $events_all = array();

            if(isset($id) && $id!=''){
                $data['id'] = $this->encryptionfunction->aes_decrypt_string($id);
            }

            if(isset($postdata['id_in']) && $postdata['id_in']!=''){
                $data['id_in'] = $postdata['id_in'];
            }

            if(isset($postdata['school']) && $postdata['school']!=''){
                $data['school'] = $postdata['school'];
            }

            $data['select'] = "id,name,status,created_date,email,mail_enable";
            $filters        = $this->EventModel->get_event_category($data);
            if(count($filters) > 0){
                foreach ($filters as $key => $value) {
                    $words   = explode(" ", $value['name']);
                    $acronym = "";
                    foreach ($words as $w) {
                        if($w =='and' || $w =='And' || $w =='&'){
                            $w[0] = 'A';
                        }
                        $acronym .= $w[0];
                    }

                    $filters[$key]['note']       = $value['id'];
                    $filters[$key]['id']         = $this->encryptionfunction->aes_encrypt_string($value['id']);
                    $filters[$key]['mail']       = $value['mail_enable'] == '1'?'Enabled':'Not Enabled';
                    $filters[$key]['shorttilte'] = strtolower($acronym).'_'.$value['id'];
                    $filters[$key]['itemName']   = $value['name'];
                }
                $response['status']   = 'success';
                $response['message']  = 'Successfully Retrived!';
                $response['data']     = $filters;
            }
        } else {
            $response['status']  = 'logout';
            $response['message'] = 'Invalid User Details';
        }
        echo json_encode($response);exit;
    }



}