import { Component,NgZone, ElementRef, ChangeDetectorRef, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, ActivatedRoute, Router } from '@angular/router';
import { filter, take, tap } from 'rxjs/operators';
import { WebapiService } from 'src/app/services/webapi.service';
import{ Common } from 'src/app/common/common';
import { EventEmitterService } from './services/event-emitter.service';
import { ToastrService } from 'ngx-toastr';
//import { BnNgIdleService } from 'bn-ng-idle';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    @ViewChild('closebuttonN') closebuttonN: any;
    @ViewChild('UserSessionExpiryButton') UserSessionExpiryButton: any;
    currentYear = new Date().getFullYear();
    title = 'rks';
    isAuthenticated = Common.userAuthenticated;
    isAuthen: boolean = false;
    isLoginDetails: any;
    is_userType: any;
    titles = Common.siteTitle;
    isOpened: boolean = true;
    isPopupLog: boolean = false;
   // profileImg: string = '/assets/panel/img/client_img.png';
    UserFullname: string = 'User';
    counterTimer: any;
    menuAccess: any;
    showLoading: boolean = false;
    pageLoading: boolean = false;
    timers: any;
  

    constructor(private webapi: WebapiService,  private route: ActivatedRoute, private router: Router, private toastr: ToastrService, private element: ElementRef, private eventEmitterService: EventEmitterService, private cdr: ChangeDetectorRef,private _ngZone: NgZone,private ngxLoader: NgxUiLoaderService) { }

    ngOnInit(): void {
        //this.isLoginDetails = this.webapi.getData();
        let currentURL      = window.location.pathname;
        this.openModal();
        this.isLoggedin();
        if ((currentURL != '/login')) {
            /*this.bnIdle.startWatching(900).subscribe((isTimedOut: boolean) => {
                if (isTimedOut) {
                     this.webapi.showError("Session Inactivity Time 15 mins Exceeded!", '');
                    this.logoutUser();
                }
            });*/
        }

        if (this.eventEmitterService.subsVar == undefined) {
            this.eventEmitterService.subsVar = this.eventEmitterService.
                invokeFirstComponentFunction.subscribe((name: string) => {
                    this.isLoggedin();
                    this.cdr.detectChanges();
                });

            this.eventEmitterService.subsVar = this.eventEmitterService.
                invokeshowCommonLoader.subscribe((name: string) => {
                    //this.showLoading = true;
                    this.ngxLoader.start();
                    this.cdr.detectChanges();
                });

            this.eventEmitterService.subsVar = this.eventEmitterService.
                invokeHideCommonLoader.subscribe((name: string) => {
                    //this.showLoading = false;
                    this.ngxLoader.stop();
                    this.cdr.detectChanges();
                });

            this.eventEmitterService.subsVar = this.eventEmitterService.
                invokeshowPageLoader.subscribe((name: string) => {
                    this.pageLoading = true;
                    this.cdr.detectChanges();
                });

            this.eventEmitterService.subsVar = this.eventEmitterService.
                invokehidePageLoader.subscribe((name: string) => {
                    this.pageLoading = false;
                    this.cdr.detectChanges();
                });
        }
    }

    public isLoggedin() {
        let loggin_status = this.webapi.loggedInUserValue();
        if (loggin_status != null) {
            this.isAuthenticated = true;
            //this.showLoading = false;
            let user_token      = localStorage.getItem(Common.user_token);
            this.isLoginDetails = JSON.stringify(user_token);
            this.isLoginDetails = JSON.parse(user_token|| '{}');
            //this.getrksusersmenu({parent_id:0});
        } else {
            this.showLoading = false;
            this.isAuthenticated = false;
            this.router.navigate(['/login']);
        }
    }

    public logoutUser(): void {
        this.webapi.logoutUser();
        this.isAuthenticated = false;
        this.showLoading     = false;
        this._ngZone.run(()=>{
            this.router.navigate(['/login']);
        });
    }

    /*public validateToken(login: any, loginRequest: string) {
        this.webapi.verifyToken()
            .subscribe(
                (data: any) => {
                    if (data == '1' || data == 1) {
                        //this.validateLogin(this.isLoginDetails, loginRequest);
                        //this.getrksusersmenu(this.isLoginDetails);
                        this.isAuthenticated = true;
                    } else {
                        this.showLoading = false;
                        this.isAuthenticated = false;
                        this.logoutUser();
                    }
                });
    }*/

    /*validateLogin(data: any, loginRequest: string) {
        this.webapi.validateToken(data)
            .subscribe(
                (data: any) => {
                    this.showLoading = false;
                    if (data.status == 'success') {
                        this.isAuthenticated = true;
                        this.is_userType = data.roletype;
                        this.profileImg = data.profile;
                        this.UserFullname = data.fullname;
                        if (loginRequest == '1') {
                            this.router.navigate(['/admin']);
                        }
                    } else {
                        this.logoutUser();
                    }
                }, error => {
                    this.showLoading = false;
                    this.webapi.showError("Request Failed!", '');
                    this.router.navigate(['/']);
                });
    }*/

    /*getrksusersmenu(data: any) {
        this.webapi.getUserMenu(data).subscribe(
            (result: any) => {
                if (result.status == 'success') {
                    this.menuAccess = result.data;
                } else if (result.status == 'logout') {
                    this.router.navigate(['/login']);
                } else {
                    this.router.navigate(['/']);
                    this.webapi.showError(result.message, '');
                }
                this.showLoading = false;
                ////console.log(result);
            }, error => {
                this.showLoading = false;
                this.webapi.showError("Request Failed!", '');
                this.router.navigate(['/']);
            });

    }*/

    goToRandomNumberPage(event: any) {
        event.preventDefault();
        this.router.navigateByUrl('/', { skipLocationChange: true })
            .then(() => this.router.navigate(['/']));
    }

    redirectmenu(menu: string) {
        //this.toggleClass();
        this._ngZone.run(()=>{
            this.router.navigate(['/' + menu]);
        });
        //this.router.navigate(['/' + menu]);
    }

    isOpen() {
        return this.isOpened;
    }

    toggleClass() {
        this.isOpened = !this.isOpened;
    }

    timer(minute: number) {
        let seconds: number = minute * 60;
        let textSec: any = "0";
        let statSec: number = 60;
        const prefix = minute < 10 ? "0" : "";
        this.timers = setInterval(() => {
            seconds--;
            if (statSec != 0) statSec--;
            else statSec = 59;

            if (statSec < 10) {
                textSec = "0" + statSec;
            } else textSec = statSec;
            this.counterTimer = `${prefix}${Math.floor(seconds / 60)}:${textSec}`;
            if (seconds == 0) {
                this.webapi.showError("Browser Inactivity Time Exceeded!", '');
                clearInterval(this.timers);
                this.onCloseModal();
                this.logoutUser();
            }
        }, 1000);
    }

    continueActivity(event: any) {
        clearInterval(this.timers);
        this.closebuttonN.nativeElement.click();
    }

    public onCloseModal() {
        this.closebuttonN.nativeElement.click();
    }

    openModal() {
        /*const buttonModal = document.getElementById("UserSessionExpiryButton")!
        buttonModal.click();*/
    }

    isActive(instruction:string) {
        if(this.router.url === '/'+instruction){
            return true;
        } else {
            return false;
        }
      }
}