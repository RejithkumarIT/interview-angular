import { Component, NgZone,ElementRef,AfterViewInit, OnInit, Input } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { filter, take, tap, switchMap } from 'rxjs/operators';
import { WebapiService } from 'src/app/services/webapi.service';
import{ Common } from 'src/app/common/common';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
declare var google:any; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,AfterViewInit {
  public isAuthenticated = false;
  currentYear            = new Date().getFullYear();
  isLoginDetails :any;
  is_userType :any;
  socialUser: any;
  constructor(private webapi: WebapiService, private route: ActivatedRoute, private router: Router,private element: ElementRef,private eventEmitter: EventEmitterService,private _ngZone: NgZone) { }
  ngAfterViewInit(): void {
    this.isLoggedin();
    let handleCredentialResponse = (response:any) => {
      const data = {idToken:response.credential,oauth:'v3'};
      this.loginCheckSocial(data);
    }
    let id = 'google-client-script';
    let script = document.getElementById(id);
    if(script === null) {
      let crscript = document.createElement('script');
      crscript.setAttribute('src', 'https://accounts.google.com/gsi/client');
      crscript.setAttribute('id', id);
      crscript.setAttribute('async', '');
      document.body.appendChild(crscript);
      crscript.onload = () => {
        google.accounts.id.initialize({
          client_id: Common.googleauthtoken,
          callback: handleCredentialResponse
        });

        google.accounts.id.renderButton(document.getElementById("buttonDiv"),{ theme: "outline", size: "large" });
        google.accounts.id.prompt();
      }
    } else {
      google.accounts.id.initialize({
        client_id: Common.googleauthtoken,
        callback: handleCredentialResponse
      });

      google.accounts.id.renderButton(document.getElementById("buttonDiv"),{ theme: "outline", size: "large" });
      google.accounts.id.prompt();
    }
  }

  ngOnInit(){
  }
  
  public loginCheckSocial(res:any)
  {
    const googleAuthorize = {gtoken:res.idToken, socialLogin:res.oauth,source:'weblogin' };
    this.eventEmitter.showCommonLoader();
    this.webapi.loginUser(googleAuthorize).subscribe(result => { 
      if (result.status == 'success') {
        this.isAuthenticated              = true;
        this.webapi.showSuccess(result.message, '');
        this.eventEmitter.onFirstComponentButtonClick();
        this._ngZone.run(()=>{
          this.router.navigate(['/events']);
        });
      } else {
        alert(result.message);
        this.webapi.showError(result.message, '');
      }
      this.eventEmitter.hideCommonLoader();
    }, error => {
      this.eventEmitter.hideCommonLoader();
      this.webapi.showError("Request Failed now!",'');
    });
  }

  public isLoggedin(){
    let loggin_status = this.webapi.loggedInUserValue();
    if(loggin_status !=null){
      this.router.navigate(['/dashboard']);
    } else {
      this.eventEmitter.onFirstComponentButtonClick();
    }              
  }
}
