import { Component, NgZone,OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import {ActivatedRoute,Router} from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { filter, take, tap, switchMap } from 'rxjs/operators';
import { WebapiService } from 'src/app/services/webapi.service';
import{ Common } from 'src/app/common/common';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
declare var $:any;
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  events:any;
  eventID:any;
  duplicate:any = {duplicateTimes:'1'};
  page:any = 1;
  pageSize:any = 5;
  totalLength:any;
  constructor(private webapi: WebapiService, private route: ActivatedRoute, private router: Router,private eventEmitterService: EventEmitterService,private _ngZone : NgZone) { }

  ngOnInit(): void {
    this.getEvents();
    /*$('#touchSpin1').TouchSpin({
        min: 0,
        max: 100,                
        boostat: 5,
        maxboostedstep: 10,        
        initval: 0
      });*/
  }

  public getEvents(id:string='') {
    this.eventEmitterService.showCommonLoader();
    let data = {source:'eventspage-reg'};
    this.webapi.getEvents(data,id).subscribe(result => {              
      if (result.status == 'success') {
        this.events = result.data;
        this.totalLength = this.events.length;

      } else {
        this.webapi.showError(result.message, '');
        
      }
      this.eventEmitterService.hideCommonLoader();
    }, error => {
      this.eventEmitterService.hideCommonLoader();
      this.webapi.showError("Request Failed now!",'');
    });
    
  }

  public deleteEvent(id:string){
    this.eventID = id;
  }

  public duplicateEvent(id:string){
    this.eventID = id;
  }

  public confirmdeleteEvent(id:string){
    let data = {};
    this.eventEmitterService.showCommonLoader();
    this.webapi.deleteEvents(data,id).subscribe((result:any) => {              
      if (result.status == 'success') {
        this.getEvents();
        this.webapi.showSuccess(result.message, '');
      } else {
        this.webapi.showError(result.message, '');
      }
      this.eventEmitterService.hideCommonLoader();
    }, error => {
      this.eventEmitterService.hideCommonLoader();
      this.webapi.showError("Request Failed now!",'');
    });
    this.eventID = '';
  }

  public confirmduplicateEvent(data:any,id:string){
    this.eventEmitterService.showCommonLoader();
    this.webapi.duplicateEvent(data,id).subscribe((result:any) => {              
      if (result.status == 'success') {
        this.getEvents();
        this.webapi.showSuccess(result.message, '');
      } else {
        this.webapi.showError(result.message, '');
      }
      this.eventEmitterService.hideCommonLoader();
    }, error => {
      this.eventEmitterService.hideCommonLoader();
      this.webapi.showError("Request Failed now!",'');
    });
    this.eventID = '';
    this.duplicate.duplicateTimes = 1;
  }

  public incCount(){
    let value = this.duplicate.duplicateTimes;
    this.duplicate.duplicateTimes = parseInt(value) + 1; 
    return this.duplicate.duplicateTimes;
  }

  public decCount(){
    let value = this.duplicate.duplicateTimes;
    this.duplicate.duplicateTimes = parseInt(value) - 1; 
    return this.duplicate.duplicateTimes;
  }

}
