import { Component, NgZone,OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import {ActivatedRoute,Router} from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { filter, take, tap, switchMap } from 'rxjs/operators';
import { WebapiService } from 'src/app/services/webapi.service';
import{ Common } from 'src/app/common/common';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { GlobalService } from 'src/app/services/global.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
declare var $: any;
@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {
  editor:any              = ClassicEditor;
  events:any              = Common.eventData;
  fileToUpload: any;
  imageUrl: any           = '/assets/img/preview.png';
  showtimeslotadd:boolean = false;
  showcontactadd:boolean  = false;
  showregfield:boolean    = false;
  config:any = {height: 500};
  eventID :any;
  isAddForm :boolean = false;
  filters:any;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  constructor(private webapi: WebapiService,private global: GlobalService, private route: ActivatedRoute, private router: Router,private eventEmitterService: EventEmitterService,private _ngZone : NgZone,private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.eventID = this.route.snapshot.paramMap.get('id');
    if(this.eventID!=''){
      if(this.eventID!='add'){
        this.getEvents(this.eventID|| '{}');
        this.isAddForm = false;
      } else {
        this.getNot();
        this.isAddForm = true;
      }
      
    } else {
      this.isAddForm = true;
      //this.router.navigate(['/']);
    }
    this.dynamicClock(1);
  }
  public shownewFiled(fieldName:any){
    if(fieldName == 'timeslot'){
      this.events.timeslot.push({date: "",time:"10:00"});
      this.dynamicClock(this.events.timeslot.length);
    } else if(fieldName == 'contact'){
      this.events.contact.push({name: "",email:"",mobile:""});
    } 
  }

  removevalue(i:number,fieldName:string){
    if(fieldName == 'timeslot'){
      this.events.timeslot.splice(i,1);
      this.dynamicClock(this.events.timeslot.length);
    } else if(fieldName == 'contact'){
      this.events.contact.splice(i,1);
    }
  }

  public modifytimeslot(event:any){
    let value = event.target.value;
    if(value=='2'){
      this.showtimeslotadd = true;
    } else {
      this.showtimeslotadd = false;
    }
  }

  public modifyeventtype(event:any){
    let value = event.target.checked;
    if(value === true){
      this.showregfield   = true;
      this.events.is_link = '1';
    } else {
      this.showregfield = false;
      this.events.is_link = '2';
    }
  }

  public handleFileInput(event: any) {
    const files       = event.target.files;
    this.fileToUpload = files.item(0);

    //Show image preview
    let reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl     = event.target.result;
      this.events.image = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  public saveEvent(data: any) {
    this.eventEmitterService.showCommonLoader();
    this.webapi.saveEvents(data,this.eventID).subscribe(result => {              
      if (result.status == 'success') {
        this.webapi.showSuccess(result.message, '');
        this._ngZone.run(()=>{
            this.router.navigate(['/events']);
        });
      } else {
        this.webapi.showError(result.message, '');
        
      }
      this.eventEmitterService.hideCommonLoader();
    }, error => {
      this.eventEmitterService.hideCommonLoader();
      this.webapi.showError("Request Failed now!",'');
    });
    
  }

   public getNot(id:string='') {
    let data = {source:'adminpage-reg'};
    this.eventEmitterService.showCommonLoader();
    this.webapi.getNotification(data,id).subscribe(result => {              
      if (result.status == 'success') {
          this.filters  = result.data;
      } else {
        this.webapi.showError(result.message, '');
        
      }
      this.eventEmitterService.hideCommonLoader();
    }, error => {
      this.eventEmitterService.hideCommonLoader();
      this.webapi.showError("Request Failed now!",'');
    });
    
  }

  public getEvents(id:string='') {
    this.eventEmitterService.showCommonLoader();
    let data = {source:'eventspage-reg'};
    this.webapi.getEvents(data,id).subscribe(result => {              
      if (result.status == 'success') {
        this.events = result.data[0];
        if(this.events.event_timeslot=='2'){
          this.showtimeslotadd = true;
        } else {
          this.showtimeslotadd = false;
        }

        if(this.events.event_type !='2'){
          this.showregfield = true;
        } else {
          this.showregfield = false;
        }
        //this.webapi.showSuccess(result.message, '');
        /*this._ngZone.run(()=>{
            this.router.navigate(['/faculty']);
        });*/
      } else {
        this.webapi.showError(result.message, '');
        
      }
      this.eventEmitterService.hideCommonLoader();
    }, error => {
      this.eventEmitterService.hideCommonLoader();
      this.webapi.showError("Request Failed now!",'');
    });
    
  }

  sanitize(value: string,type:string='') {
    switch (type) {
      case 'html':
        return this.sanitizer.bypassSecurityTrustHtml(value);
      case 'style':
        return this.sanitizer.bypassSecurityTrustStyle(value);
      case 'script':
        return this.sanitizer.bypassSecurityTrustScript(value);
      case 'url':
        return this.sanitizer.bypassSecurityTrustUrl(value);
      case 'resourceUrl':
        return this.sanitizer.bypassSecurityTrustResourceUrl(value);
      default:
        throw new Error(`Invalid safe type specified: ${type}`);
    }
  }

  public dynamicClock(count:number){
    for(var i=0;i<=count;i++){
      $('#clockPicker2'+i).clockpicker({
        autoclose: true
      });
    }
  }
}
