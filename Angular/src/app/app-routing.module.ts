import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/services/auth.guard';
import { LoginComponent } from './components/login/login.component';
import { EventsComponent } from './components/events/events.component';
import { CreateEventComponent } from './components/create-event/create-event.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const routes: Routes = [{ path: '', component: LoginComponent},
  { path: 'login', component: LoginComponent},
  { path: 'create-event/:id', component: CreateEventComponent, canActivate: [AuthGuard]  },
  { path: 'events', component: EventsComponent, canActivate: [AuthGuard]  },
  {path: '**', component: PageNotFoundComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
