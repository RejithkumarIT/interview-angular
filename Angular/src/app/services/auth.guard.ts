import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { WebapiService } from 'src/app/services/webapi.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate{
  constructor( private webapi: WebapiService, private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        const currentUser = this.webapi.loggedInUserValue();
        if (currentUser!=null) {
            return true;
        } else {
            this.router.navigate(['/'], { queryParams: { returnUrl: state.url }});
            return false;
        }
    }
}