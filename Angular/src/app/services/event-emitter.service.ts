import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {
  invokeFirstComponentFunction = new EventEmitter();
  invokeshowCommonLoader       = new EventEmitter();
  invokeHideCommonLoader       = new EventEmitter();
  invoketokenStatus            = new EventEmitter();
  invokeshowPageLoader         = new EventEmitter();
  invokehidePageLoader         = new EventEmitter();
  subsVar: any;
  constructor() { }
  onFirstComponentButtonClick() {    
    this.invokeFirstComponentFunction.emit();    
  }

  public showCommonLoader(){
    this.invokeshowCommonLoader.emit(); 
  }

  public hideCommonLoader(){
    this.invokeHideCommonLoader.emit(); 
  }

  public showPageLoader(){
    this.invokeshowPageLoader.emit(); 
  }

  public hidePageLoader(){
    this.invokehidePageLoader.emit(); 
  }

  public tokenStatus(){
    this.invoketokenStatus.emit(); 
  }
}
