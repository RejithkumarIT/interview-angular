import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable,of, pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import{ Common } from 'src/app/common/common';

const baseUrl          = Common.baseUrl;
const login            = baseUrl+'api/login';
const extend_user      = baseUrl+'api/extend_user';
const verifytoken      = baseUrl+'api/verifyTokenData';
const getevents        = baseUrl+'api/getEvents';
const saveevents       = baseUrl+'api/saveEvents';
const deleteevents     = baseUrl+'api/deleteEvents';
const duplicateEvent   = baseUrl+'api/duplicateEvents';
const getnotif         = baseUrl+'api/getEventNotification';

@Injectable({ providedIn: 'root' })
export class WebapiService {
  private loggedUserSubject: BehaviorSubject<any>;
  public loggedInUser: Observable<any>;
  public getLoggedUser:any;
  public decodedToken: any;
  public expirationDate: any;
  public isExpired:boolean      = false;
  public isUserLoggedIn:boolean = false;
  public helper                 = new JwtHelperService();
  constructor(private httpClient: HttpClient,private toastr: ToastrService) {
    this.getLoggedUser     = localStorage.getItem(Common.token_name);
    this.loggedUserSubject = new BehaviorSubject(this.getLoggedUser);
    this.loggedInUser      = this.loggedUserSubject.asObservable();
  }

  public loginUser(data:any) {
    return this.httpClient.post<any>(`${login}/`, data)
    .pipe(map(response=> {
      if(response.status=='success'){
        this.isUserLoggedIn = true;
        localStorage.setItem(Common.token_name, JSON.stringify(response.token));
        localStorage.setItem(Common.user_token, JSON.stringify(response.data));
        this.loggedUserSubject.next(response.token);
        return response;
      }else {
        this.showError(response.message, '');
        return response;
      } 
    }));
  }

  public extendUserToken() {
    return this.httpClient.get<any>(extend_user).subscribe(response=> {
      if(response.status=='success'){
        this.isUserLoggedIn = true;
        localStorage.setItem(Common.token_name, JSON.stringify(response.token));
        this.loggedUserSubject.next(response.token);
        return response;
      }else {
        return response;
      }
    });
  }

  logoutUser() {
    localStorage.removeItem(Common.token_name);
    localStorage.removeItem(Common.user_token);
    this.loggedUserSubject.next(null);
    this.toastr.success('Successfully LoggedOut!','');
    return this.loggedInUserValue();
  }


  public userLoggedinStatus(){
    return this.httpClient.get<any>(verifytoken).subscribe(result => {
      if(result.status=='success'){
        return this.loggedUserSubject.value;
      } else {
        this.isUserLoggedIn = false;
        localStorage.removeItem(Common.token_name);
        localStorage.removeItem(Common.user_token);
        this.loggedUserSubject.next(null);
      }
    });
  }

  public loggedInUserValue(){ 
    this.decodeToken();
    return this.loggedUserSubject.value;
  }

  public decodeToken(): void {
    let tkn = localStorage.getItem(Common.token_name);
    if(tkn) {
      this.getLoggedUser  = tkn;
      this.decodedToken   = this.helper.decodeToken(tkn);
      this.expirationDate = this.helper.getTokenExpirationDate(tkn);
      this.isExpired      = this.helper.isTokenExpired(tkn);
      let expiry          = (JSON.parse(atob(tkn.split('.')[1]))).exp;
      let test_exp        = (Math.floor((new Date).getTime() / 1000));
      let check_exp       = (expiry) - (test_exp);

      if(check_exp > 0 && check_exp < Common.token_expiry_check){
        let toke = this.extendUserToken();
      } 

      if(this.isExpired){
        this.toastr.error('Session Token Expired!','');
        this.isUserLoggedIn = false;
        localStorage.removeItem(Common.token_name);
        this.loggedUserSubject.next(null);
      }
    } else {
      this.isUserLoggedIn = false;
      localStorage.removeItem(Common.token_name);
      this.loggedUserSubject.next(null);
    }
  }


  

  public getEvents(data:any,id:string=''){
    return this.httpClient.post<any>(`${getevents}/`+id, data)
    .pipe(map(response=> {
      return response;
    }));
  }

  public deleteEvents(data:any,id:string=''){
    return this.httpClient.post<any>(`${deleteevents}/`+id, data)
    .pipe(map(response=> {
      return response;
    }));
  }

  public saveEvents(data:any,id:string=''){
    return this.httpClient.post<any>(`${saveevents}/`+id, data)
    .pipe(map(response=> {
      return response;
    }));
  }


  public getNotification(data:any,id:string=''){
    return this.httpClient.post<any>(`${getnotif}/`+id, data)
    .pipe(map(response=> {
      return response;
    }));
  }


  public duplicateEvent(data:any,id:string=''){
    return this.httpClient.post<any>(`${duplicateEvent}/`+id, data)
    .pipe(map(response=> {
      return response;
    }));
  }

  

  /*toaster*/
  showSuccess(message:string, title:string){
    return this.toastr.success(message, title)
  }
  
  showError(message:string, title:string){
    return this.toastr.error(message, title)
  }
  
  showInfo(message:string, title:string){
    return this.toastr.info(message, title)
  }
  
  showWarning(message:string, title:string){
    return  this.toastr.warning(message, title)
  }
}