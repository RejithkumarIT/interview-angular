import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { WebapiService } from 'src/app/services/webapi.service';
import{ Common } from 'src/app/common/common';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private webapi: WebapiService) { }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        let loggedInUser = this.webapi.loggedInUser;
        let token = localStorage.getItem(Common.token_name);
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${JSON.parse(token)}`
                }
            });
        }
        return next.handle(request);
    }
}

